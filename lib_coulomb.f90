MODULE coulomb
!
!  Purpose:
!    Construct and manipulate the Coulomb tensor
!
!  Record of revisions:
!      Date       Programmer	Description of change
!      ====       ==========	=====================
!    11/08/14     S. Barthel	Original code
!

USE constants
USE shared_data
USE math

IMPLICIT NONE

! Data dictionary: declare constants

! Data dictionary: declare variable types, definitions, units

CONTAINS

  !=============================================================================================================================

  SUBROUTINE construct_coulombtensor( U_vasp, J_vasp )

    IMPLICIT NONE

    ! Data dictionary: declare constants

    ! Data dictionary: declare variable types, definitions, units

    REAL, INTENT(IN) :: U_vasp, J_vasp					!Averaged U,J parameters as used in VASP

    INTEGER :: ioerror							!error code for file IO
    INTEGER :: allocerror						!error code for allocation

    REAL, DIMENSION(0:6) :: F_Slater					!Slater integrals
    INTEGER :: i,j,k,l
    INTEGER :: m1, m2, m3, m4
    COMPLEX, ALLOCATABLE, DIMENSION(:,:) :: T_sph2cub			!Basis transformation from spherical to cubic harmonics

    REAL :: U_0, J_1, J_2, J_3, J_4

    REAL :: testt

    !---------------------------------------------------------------------------------------------------------------------------

!     testt = wigner_3j( 2, 6, 4, 0, 0, 0)
!
!     WRITE(6,*) testt, SQRT(715.)/143
!
!     WRITE(6,*) gaunt(1,0,1,1,0,-1), 1./(2*SQRT(PI))

    !WRITE(6,*) factorial(-7), factorial(1), factorial(13)

    !STOP

    !TO DO: check gaunt sign, function for radial F, Full-Coulomb matrix with spin -> volle HF-Selbstenergie
    IF ( myrank .EQ. 0 ) THEN
      WRITE(6,*) '  L=', L_ang
      WRITE(6,*) '  U_vasp=', U_vasp
      WRITE(6,*) '  J_vasp=', J_vasp
    ENDIF

    !Effective Slater integrals expressed by averaged U,J parameters
    F_Slater = radial_F(U_vasp,J_vasp)

!     radial_F = 0.
!     radial_F(0) = U_vasp
!     radial_F(2) = 14*J_vasp / 1.625
!     radial_F(4) = 0.625 * radial_F(2)
!
!     !Add test lines here ...
!
!     U_0 = radial_F(0) + (8./7)*(radial_F(2)+radial_F(4))/14
!     J_1 = (3./49)*radial_F(2) + (20./(9*49))*radial_F(4)
!     J_2 = (-10./7)*(radial_F(2)+radial_F(4))/14 + 3*J_1
!     J_3 = (30./7)*(radial_F(2)+radial_F(4))/14 - 5*J_1
!     J_4 = (20./7)*(radial_F(2)+radial_F(4))/14 - 3*J_1
!
!     WRITE(6,*) U_0
!     WRITE(6,*) U_0-2*J_1
!     WRITE(6,*) U_0-2*J_2
!     WRITE(6,*) U_0-2*J_3
!     WRITE(6,*) U_0-2*J_4

    !===========================================================================================================================
    !Spherical Coulomb-tensor
    !===========================================================================================================================
    ALLOCATE ( Uijkl_spherical(2*L_ang+1,2*L_ang+1,2*L_ang+1,2*L_ang+1), STAT=allocerror )
    IF ( allocerror > 0 ) THEN
      WRITE(*,*) 'ERROR: Memory for "Uijkl_spherical" could not be allocated'
      STOP
    ELSE
      !null
      Uijkl_spherical = 0.
    ENDIF

    IF ( myrank .EQ. 0 ) THEN
      WRITE(6,*) '  Constructing spherical Coulomb-tensor...'
    ENDIF

    DO m1=-L_ang,L_ang
      DO m2=-L_ang,L_ang
	DO m3=-L_ang,L_ang
	  DO m4=-L_ang,L_ang

            !-----------------------------------------------------------------------------------------------------------------------
            !Equation (10) of 1997 J.Phys.: Condens Matter 9 767, V. I. Anisimov, F. Aryasetiawan, A.I. Lichtenstein
            !Operator ordering: c+_i c_l c+_j c_k = c+_i c+_j c_k c_l, Integral ordering: U_ijlk, last two indices differ !
            !-----------------------------------------------------------------------------------------------------------------------
! 	    DO k=0,(2*L_ang),2
! 	      Uijkl_spherical(m1+L_ang+1,m3+L_ang+1,m2+L_ang+1,m4+L_ang+1) = &
! 	      Uijkl_spherical(m1+L_ang+1,m3+L_ang+1,m2+L_ang+1,m4+L_ang+1) + radial_F(k)*angular_a(k,L_ang,m1,m2,m3,m4)
! 	    ENDDO

	    !Last two indices interchanged V_ijlk -> V_ijkl
	    DO k=0,(2*L_ang),2
	      Uijkl_spherical(m1+L_ang+1,m3+L_ang+1,m4+L_ang+1,m2+L_ang+1) = &
	      Uijkl_spherical(m1+L_ang+1,m3+L_ang+1,m4+L_ang+1,m2+L_ang+1) + F_Slater(k)*angular_a(k,L_ang,m1,m2,m3,m4)
	    ENDDO

	    !-----------------------------------------------------------------------------------------------------------------------
	  ENDDO
	ENDDO
      ENDDO
    ENDDO

!     !export
!     OPEN (UNIT=26,FILE='./Uijkl_sph.txt',STATUS='REPLACE', ACTION='WRITE', IOSTAT=ioerror)
!     DO m1=-L_ang,L_ang
!       DO m2=-L_ang,L_ang
! 	DO m3=-L_ang,L_ang
! 	  DO m4=-L_ang,L_ang
! 	    WRITE(26,'(3X,I2,1X,I2,1X,I2,1X,I2,3X,F19.16,1X,F19.16)') m1+L_ang+1,m2+L_ang+1,m3+L_ang+1,m4+L_ang+1, &
! 	    Uijkl_spherical(m1+L_ang+1,m2+L_ang+1,m3+L_ang+1,m4+L_ang+1)
! 	  ENDDO
! 	ENDDO
!       ENDDO
!     ENDDO
!     CLOSE(26)

    !===========================================================================================================================
    !Cubic Coulomb-tensor
    !===========================================================================================================================

    !Transformation to cubic harmonics (only d-electrons atm., VASP order, xy yz z2 xz x²-z²)
    ALLOCATE ( T_sph2cub(2*L_ang+1,2*L_ang+1), STAT=allocerror )
    IF ( allocerror > 0 ) THEN
      WRITE(*,*) 'ERROR: Memory for "T_sph2cub" could not be allocated'
      STOP
    ELSE
      !null
      T_sph2cub = CMPLX(0.,0.)
      IF (L_ang .EQ. 2) THEN
          !5 d-orbitals: xy yz z2 xz x2-y2
          T_sph2cub(1,1) = CMPLX(0.,1./SQRT(2.))
          T_sph2cub(1,5) = -CMPLX(0.,1./SQRT(2.))
          T_sph2cub(2,2) = CMPLX(0.,1./SQRT(2.))
          T_sph2cub(2,4) = CMPLX(0.,1./SQRT(2.))
          T_sph2cub(3,3) = CMPLX(1.,0.)
          T_sph2cub(4,2) = CMPLX(1./SQRT(2.),0.)
          T_sph2cub(4,4) = -CMPLX(1./SQRT(2.),0.)
          T_sph2cub(5,1) = CMPLX(1./SQRT(2.),0.)
          T_sph2cub(5,5) = CMPLX(1./SQRT(2.),0.)
       ELSEIF (L_ang .EQ. 1) THEN
          T_sph2cub(1,1) = CMPLX(0.,1./SQRT(2.))
          T_sph2cub(1,3) = CMPLX(0.,1./SQRT(2.))
          T_sph2cub(2,2) = CMPLX(1.,0.)
          T_sph2cub(3,1) = CMPLX(1./SQRT(2.),0.)
          T_sph2cub(3,3) = -CMPLX(1./SQRT(2.),0.)
       ELSEIF (L_ang .EQ. 0) THEN
          T_sph2cub(1,1) = CMPLX(1.0,0.0)
       ELSE

          IF ( myrank .EQ. 0 ) THEN
             WRITE(6,*) '  sph2cub only implemented for L=0,1,2 ..'
             CALL EXIT()
          ENDIF
        ENDIF

    ENDIF

    IF ( myrank .EQ. 0 ) THEN
      WRITE(6,*) '  Transforming to cubic Coulomb-tensor...'
      WRITE(6,*) '-'
    ENDIF

    ALLOCATE ( Uijkl_cubic(2*L_ang+1,2*L_ang+1,2*L_ang+1,2*L_ang+1), STAT=allocerror )
    IF ( allocerror > 0 ) THEN
      WRITE(*,*) 'ERROR: Memory for "Uijkl_cubic" could not be allocated'
      STOP
    ELSE
      !null
      Uijkl_cubic = CMPLX(0.,0.)
    ENDIF

    DO i=1,2*L_ang+1
      DO j=1,2*L_ang+1
	DO k=1,2*L_ang+1
	  DO l=1,2*L_ang+1

	    DO m1=1,2*L_ang+1
	      DO m2=1,2*L_ang+1
		DO m3=1,2*L_ang+1
		  DO m4=1,2*L_ang+1
		    Uijkl_cubic(i,j,k,l) = Uijkl_cubic(i,j,k,l) + CONJG(T_sph2cub(i,m1))*CONJG(T_sph2cub(j,m2)) &
		      * Uijkl_spherical(m1,m2,m3,m4) * T_sph2cub(k,m3) * T_sph2cub(l,m4)
		  ENDDO
		ENDDO
	      ENDDO
	    ENDDO

	  ENDDO
	ENDDO
      ENDDO
    ENDDO

    DEALLOCATE(T_sph2cub)

!     !export
!     OPEN (UNIT=26,FILE='./Uijkl_cub.txt',STATUS='REPLACE', ACTION='WRITE', IOSTAT=ioerror)
!     DO i=1,2*L_ang+1
!       DO j=1,2*L_ang+1
! 	DO k=1,2*L_ang+1
! 	  DO l=1,2*L_ang+1
! 	    WRITE(26,'(3X,I2,1X,I2,1X,I2,1X,I2,1X,F19.16,1X,F19.16)'), i,j,k,l, Uijkl_cubic(i,j,k,l)
! 	  ENDDO
! 	ENDDO
!       ENDDO
!     ENDDO
!     CLOSE(26)

  END SUBROUTINE construct_coulombtensor

  SUBROUTINE read_coulombtensor( )

    IMPLICIT NONE

    INTEGER :: ioerror							!error code for file IO
    INTEGER :: allocerror						!error code for allocation


    INTEGER :: i,j,k,l
    INTEGER :: m1
    REAL :: tempR, tempI

    ALLOCATE ( Uijkl_cubic(2*L_ang+1,2*L_ang+1,2*L_ang+1,2*L_ang+1), STAT=allocerror )
    ALLOCATE ( Uijkl_spherical(2*L_ang+1,2*L_ang+1,2*L_ang+1,2*L_ang+1), STAT=allocerror )

    OPEN (UNIT=26,FILE='./UIJKL',STATUS='OLD', ACTION='READ', IOSTAT=ioerror)

    DO m1=1,(2*L_ang+1)**4
        READ(26,*) i,j,k,l,tempR,tempI
        Uijkl_cubic(i,j,k,l) = CMPLX(tempR,tempI)
    ENDDO
    CLOSE(26)

  END SUBROUTINE read_coulombtensor

  !=============================================================================================================================

  FUNCTION radial_F( U, J )
    !
    !  Purpose:
    !    Calculate the radial effective Slater integrals F^k for d-orbitals, L=2
    !
    !  Record of revisions:
    !      Date       Programmer	Description of change
    !      ====       ==========	=====================
    !    31/05/15     S. Barthel	Original code
    !
    IMPLICIT NONE

    ! Data dictionary: declare constants

    ! Data dictionary: declare variable types, definitions, units
    REAL, INTENT(IN) :: U, J

    REAL, DIMENSION(0:6) :: radial_F

    !Effective Slater integrals expressed by averaged U,J parameters (Coulomb U, Stoner J)
    radial_F = 0.

    radial_F(0) = U
    radial_F(2) = 14*J / 1.625
    radial_F(4) = 0.625 * radial_F(2)

  END FUNCTION radial_F

  !=============================================================================================================================

  FUNCTION angular_a( k, l, m1, m2, m3, m4 )
    !
    !  Purpose:
    !    Calculate the angular Slater integrals a_k(m1,m2,m3,m4)
    !
    !  Record of revisions:
    !      Date       Programmer	Description of change
    !      ====       ==========	=====================
    !    31/05/15     S. Barthel	Original code
    !
    IMPLICIT NONE

    ! Data dictionary: declare constants

    ! Data dictionary: declare variable types, definitions, units
    INTEGER, INTENT(IN) :: k, l
    INTEGER, INTENT(IN) :: m1, m2, m3, m4

    REAL :: angular_a

    INTEGER :: q
    REAL :: sgn

    angular_a = 0.
    DO q=-k,k
      IF ( MOD(m1+m3+q,2) /= 0 ) THEN
        sgn = -1.
      ELSE
        sgn = 1.
      ENDIF
      angular_a = angular_a + sgn * gaunt( l, k, l, -m1, q, m2 ) * gaunt( l, k, l, -m3, -q, m4 )
    ENDDO
    angular_a = (4*PI/(2*k+1)) * angular_a

  END FUNCTION angular_a

  !=============================================================================================================================

END MODULE coulomb
