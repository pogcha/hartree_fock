MODULE shared_data
  !
  !  Purpose:
  !    To declare data to share between routines
  !
  !  Record of revisions:
  !      Date       Programmer	Description of change
  !      ====       ==========	=====================
  !    27/04/15     S. Barthel	Original code
  !

  USE greens_functions

  IMPLICIT NONE
  SAVE

  ! Data dictionary: declare constants

  ! Data dictionary: declare variable types, definitions, units

  !=================================================================================================================================
  !Parameters (Valus within this section are set as default values if not specified within "HF_PARA_INPUT")
  !=================================================================================================================================

  LOGICAL :: T0 = .TRUE.                      !Do T=0 calculation
  REAL :: beta = 100.0				               !Inverse temperature

  INTEGER :: n_iw = 2100				       !Number of Matsubara frequencies

  INTEGER :: n_iter = 5                                        !Maximum number of iterations

  REAL :: mix_new_sigma = 0.5			               !mixing factor for new self-energy

  REAL :: idelta = 0.05                                        !Lorentzian broadening of the DOS lines

  REAL :: mu_dftnew

  REAL :: mu_dc

  INTEGER :: dos_N = 10001

  REAL :: dos_min = -20.0

  REAL :: dos_max = 10.0

  !---------------------------------------------------------------------------------------------------------------------------------
  !Coulomb-Interaction as parameterized in VASP

  INTEGER :: n_ions = 1

  REAL :: U_3d = 8.0	!4.0
  REAL :: J_3d = 1.36	!0.68

  REAL :: U = 8.00
  REAL :: J = 1.00

  REAL, Allocatable, DIMENSION(:) :: M                          !magnetic energy kick

  REAL, DIMENSION(3) :: mu_int = (/-30., 0., 30./)		!nested interval for mu-search
  LOGICAL :: is_metallic = .FALSE.			        !insulator, semiconductor or metal algorithm

  LOGICAL :: use_para = .FALSE.			                !use paramagnetic self-energy

  LOGICAL :: use_t2g_eg_sym = .FALSE.		                !symmetrize density matrix for each ion and spin sector
  LOGICAL :: use_afm_sym = .FALSE.			        !symmetrize density matrix antiferromagnetically wrt. ions
  LOGICAL :: use_sym_ions = .FALSE.			        !symmetrize density matrix wrt. ions

  !---------------------------------------------------------------------------------------------------------------------------------
  REAL :: tol = 1.0D-8				                !Convergence criterion for chemical potential and energy

  LOGICAL :: calc_G0_dos_only = .FALSE.		                !flag to stop code after calculating G0 dos
  LOGICAL :: use_sc_G = .TRUE.			                !use self-consistent G
  LOGICAL :: read_sigma = .TRUE.			        !start with self-energy file in iteration 1

  LOGICAL :: export_para = .TRUE.			        !export paramagnetic self-energy

  LOGICAL :: set_mu_dft = .FALSE.                               !Set value for the chemical potential manually

  LOGICAL :: set_mu_dc = .FALSE.                                !Set value for the double-counting potential manually
  LOGICAL :: dc_FLL = .TRUE.                                    !Set value for the double-counting potential by FLL
  LOGICAL :: dc_AMF = .FALSE.                                   !Set value for the double-counting potential by AMF

  !=================================================================================================================================
  !Construction of Greens functions
  !=================================================================================================================================

  INTEGER, PARAMETER :: n_spins = 2
  INTEGER :: n_spins_H0
  INTEGER :: n_kpoints						!Number of k-points
  INTEGER :: n_bands_cr						!number of bands taken into acccount for projector construction
  INTEGER :: n_bands						!Number of bands (unused)
  INTEGER :: n_electrons					!Number of electrons in the Hamiltonian

  INTEGER :: fit_start = 100

  REAL :: mu_dft 						!Fermi-level as calculated by VASP 5.4.1, read from DOSCAR/OUTCAR

  REAL, ALLOCATABLE, DIMENSION(:) :: k_weight			!Weight of k-points (Multiplicity)
  COMPLEX, ALLOCATABLE, DIMENSION(:,:,:) :: H0			!Hamiltonian <cf|H|cf´>(k) in static (w->infty) crystal field basis
  COMPLEX, ALLOCATABLE, DIMENSION(:,:,:,:) :: S_HF, S_HF_old	!Hartree-Fock Self-energy
  COMPLEX, ALLOCATABLE, DIMENSION(:,:,:) :: density_G,density_G0		!Reduced single-particle density-matrix

  REAL, ALLOCATABLE, DIMENSION(:,:) :: N_tot, N_tot_0                   !Total number of electrons in each spin-channel

  REAL :: mu, mu_old, delta_mu					!chemical potential

  TYPE( gf_iw ) :: G_HKcf

  REAL, ALLOCATABLE, DIMENSION(:) :: E_dc_fll    		!double-counting energy (to be subtracted from DFT total energy)

  REAL :: Epot, Epot_old, delta_Epot,E_kin				!Potential energy arising from beyond-DFT interactions

  !===============================================================================================================================
  !COULOMB TENSOR
  !===============================================================================================================================
  INTEGER :: L_ang = 2				!Angular momentum L quantum number (d=2,f=3)
  INTEGER :: L_max 			!magnetic quantum number m
  REAL, ALLOCATABLE, DIMENSION(:,:,:,:) :: Uijkl_spherical	!Coulomb-tensor in basis of spherical harmonics
  COMPLEX, ALLOCATABLE, DIMENSION(:,:,:,:) :: Uijkl_cubic	!Coulomb-tensor in basis of cubic harmonics
  LOGICAL :: read_coulomb = .FALSE.
  !L_max = 2*L_ang+1

END MODULE shared_data
