MODULE import
  !
  !  Purpose:
  !    Import external input
  !
  !  Record of revisions:
  !      Date       Programmer	Description of change
  !      ====       ==========	=====================
  !    23/09/14     S. Barthel	Original code
  !

  USE shared_data
  USE fileio

  IMPLICIT NONE

  ! Data dictionary: declare constants

  ! Data dictionary: declare variable types, definitions, units

  CONTAINS

  !=============================================================================================================================

  SUBROUTINE import_H_fBZ

    USE parallel

    IMPLICIT NONE

    ! Data dictionary: declare constants

    ! Data dictionary: declare variable types, definitions, units

    INTEGER :: ioerror, allocerror				!error codes for allocation and file IO
    INTEGER :: spin, kpoint, orbital, orbital_			!quantum numbers
    REAL :: tmp_real , tmp_imag					!temporary read in variables

    INTEGER :: tmp_spin, tmp_kpoint

    COMPLEX, ALLOCATABLE, DIMENSION(:,:,:,:) :: H0_spin		!Hamiltonian <cf|H|cf´>(k) in static (w->infty) crystal field basis

    !===========================================================================================================================
    !Import Hamiltonian <cr|H|cr´>(k) with orthonormalized projectors for ctqmc
    !===========================================================================================================================

    2001 FORMAT(4(2X,I6),2X,'# of spins, # of k-points, # of orbitals, # of electrons')
    !2002 FORMAT(2(2X,I6))
    2002 FORMAT(2(2X,I6),2X,F19.15)
    2003 FORMAT(2(2X,F19.15))

    2204 FORMAT(3X,"H_fBZ.proj data present:"/ &
      ,3X,'->',3X,I4,4X,"n-spins              (s)"/&
      ,3X,'->',2X,I5,4X,"k-points             (k)"/&
      ,3X,'->',3X,I4,4X,"projected orbitals  (cr)"/&
      ,3X,'->',3X,I4,4X,"initial DFT-bands    (n)"/&
      ,3X,'->',3X,I4,4X,"number of electrons (e-)")

    OPEN (UNIT=26,FILE='./H_fBZ.proj',STATUS='OLD', ACTION='READ', IOSTAT=ioerror)

      IF ( ioerror /= 0 ) THEN
        WRITE(6,*) 'ERROR: H_fBZ.proj file missing'
        CALL MPI_Abort(MPI_COMM_WORLD, i_error)
        STOP
      ENDIF
    READ(26,*) n_spins_H0, n_kpoints, n_bands_cr, n_electrons

    n_bands = -1

    IF ( myrank .EQ. 0 ) THEN
      WRITE(6,2204) n_spins_H0, n_kpoints, n_bands_cr, n_bands, n_electrons
    ENDIF

    !Allocate Hamiltonian
    ALLOCATE ( k_weight(n_kpoints), STAT=allocerror )
    IF ( allocerror > 0 ) THEN
      WRITE(6,*) 'ERROR: Memory for "k_weight" could not be allocated'
      STOP
    ELSE
      k_weight = 0.
    ENDIF

    !Allocate Hamiltonian
    ALLOCATE ( H0_spin(n_bands_cr,n_bands_cr,n_kpoints,n_spins_H0), STAT=allocerror )
    IF ( allocerror > 0 ) THEN
      WRITE(6,*) 'ERROR: Memory for "HKcr" could not be allocated'
      STOP
    ELSE
      H0_spin = CMPLX(0.,0.)
    ENDIF

    DO spin=1,n_spins_H0
    DO kpoint=1,n_kpoints
      !READ(26,2002) tmp_spin, tmp_kpoint, k_weight(kpoint)
      READ(26,*)
      k_weight(kpoint) = 1.0
    !WRITE(6,*) 'kk', tmp_spin, tmp_kpoint,k_weight(kpoint)
    DO orbital=1,n_bands_cr
    DO orbital_=1,n_bands_cr

      IF ( orbital_ == n_bands_cr ) THEN
	READ(26, 2003) tmp_real, tmp_imag
      ELSE
	READ(26, 2003, ADVANCE='NO') tmp_real, tmp_imag
      ENDIF
      !WRITE(6,*) 'kk', kpoint, orbital, orbital_, tmp_real, tmp_imag
      H0_spin(orbital,orbital_,kpoint, spin) = CMPLX(tmp_real, tmp_imag)

    ENDDO
    ENDDO

    ENDDO
    ENDDO
    CLOSE(26)

    !Allocate Hamiltonian
    ALLOCATE ( H0(n_bands_cr,n_bands_cr,n_kpoints), STAT=allocerror )
    IF ( allocerror > 0 ) THEN
      WRITE(6,*) 'ERROR: Memory for "HKcr" could not be allocated'
      STOP
    ELSE
      H0 = SUM(H0_spin,4) / n_spins_H0	!paramagnetic input
      !k_weight = k_weight / SUM( k_weight )
    ENDIF

    DEALLOCATE ( H0_spin, STAT=allocerror )
    IF ( allocerror > 0 ) THEN
      WRITE(*,*) 'ERROR: Memory for "H0_spin" could not be deallocated'
      STOP
    ENDIF

    IF ( myrank .EQ. 0 ) THEN
      WRITE(6,*) '  Hamiltonian <cr|H(k)|cr´> import done (paramagnetic).'
      WRITE(6,*) '-'
    ENDIF

  END SUBROUTINE import_H_fBZ

  !===========================================================================================================================

  SUBROUTINE import_HKcf_ctqmc

    USE parallel

    IMPLICIT NONE

    ! Data dictionary: declare constants

    ! Data dictionary: declare variable types, definitions, units

    INTEGER :: ioerror, allocerror					!error codes for allocation and file IO
    INTEGER :: kpoint, orbital, orbital_prime				!quantum numbers
    REAL :: tmp_real , tmp_imag					!temporary read in variables

    !===========================================================================================================================
    !Export Hamiltonian <cr|H|cr´>(k) with orthonormalized projectors for ctqmc
    !===========================================================================================================================

    !header version 1
    !4310 FORMAT (2X,I8,2X,I4,2X,I4,1X,'#k-points #bands_cr #bands')
    4310 FORMAT (2X,I8,2X,I4,2X,I4,27X)	!gfortran


!     !header version 2
!     4308 FORMAT(2X,'VERSION 2')
!     4309 FORMAT(2X,I8,2X,I4) 			!n_kpoints, n_ions_c
!     4310 FORMAT(2X,I8,2X,I8,2X,I8)		!n_d, n_p, n_ligands

    !hamiltonian format

    4311 FORMAT(2X,F11.8,2X,F11.8,2X,F11.8)
    4312 FORMAT(2X,F19.15,2X,F19.15)

    2200 FORMAT(3X,"HKcf data present:"/ &
      ,3X,'->',2X,I5,4X,"k-points            (k)"/&
      ,3X,'->',3X,I4,4X,"projected orbitals (cf)"/&
      ,3X,'->',3X,I4,4X,"initial DFT-bands   (n)")

    OPEN (UNIT=26,FILE='./HKcf_CTQMC.txt',STATUS='OLD', ACTION='READ', IOSTAT=ioerror)
    READ(26,4310) n_kpoints, n_bands_cr, n_bands !header line OBSOLETE

    IF ( myrank .EQ. 0 ) THEN
      WRITE(6,2200) n_kpoints, n_bands_cr, n_bands
    ENDIF

    !Allocate Hamiltonian
    ALLOCATE ( H0(n_bands_cr,n_bands_cr,n_kpoints), STAT=allocerror )
    IF ( allocerror > 0 ) THEN
      WRITE(6,*) 'ERROR: Memory for "HKcf" could not be allocated'
      STOP
    ELSE
      H0 = CMPLX(0.,0.)
    ENDIF

    DO kpoint=1,n_kpoints
      !WRITE(26,4311) kpoints(kpoint,:)
      READ(26,4311)
      DO orbital=1,n_bands_cr
	DO orbital_prime=1,n_bands_cr
	  IF ( orbital_prime == n_bands_cr ) THEN
	    READ(26, 4312) tmp_real, tmp_imag
	  ELSE
	    READ(26, 4312, ADVANCE='NO') tmp_real, tmp_imag
	  ENDIF
	  H0(orbital,orbital_prime,kpoint) = CMPLX(tmp_real, tmp_imag)
	ENDDO
      ENDDO
    ENDDO

    CLOSE(26)
    IF ( myrank .EQ. 0 ) THEN
      WRITE(6,*) '  Hamiltonian <cf|H(k)|cf´> import done.'
      WRITE(6,*) '-'
    ENDIF

  END SUBROUTINE import_HKcf_ctqmc

  !============================================================================================================================

  SUBROUTINE import_vasp_outcar_mu_dft

    IMPLICIT NONE

    ! Data dictionary: declare constants

    ! Data dictionary: declare variable types, definitions, units

    INTEGER :: ioerror, allocerror    !error codes for allocation and file IO

    INTEGER :: line_count, i

    INTEGER, PARAMETER :: n_backspace = 30      !line from eof containing the Fermi-energy
    INTEGER, PARAMETER :: idx_start_fermi = 23  !start character index of Fermi-energy
    INTEGER, PARAMETER :: idx_end_fermi = 32    !end character index of Fermi-energy

    !===========================================================================================================================


    OPEN (UNIT=26,FILE='./OUTCAR',STATUS='OLD', ACTION='READ', IOSTAT=ioerror)
    !Check for the correct OUTCAR file
      IF ( ioerror /= 0 ) THEN
        WRITE(6,"(3X,'OUTCAR file is missing.')")
        WRITE(6,"(3X,'Please enter Fermi-energy:',1X)",ADVANCE='NO')
        READ(*,*) mu_dft
        WRITE(6,"(3X,'-> Fermi-energy:',1X,F10.6,1X,'eV')") mu_dft

      ELSE
        IF ( rank_0 ) THEN
          WRITE(6,*) '  Reading in OUTCAR ...'
        ENDIF

        !read until EOF
        ioerror = 0
        line_count = 0
        DO WHILE (ioerror .EQ. 0)
          line_count = line_count + 1
          READ(26,*,IOSTAT=ioerror)
        ENDDO
        !WRITE(6,*) line_count

        DO i=1,n_backspace
          BACKSPACE(26)
        ENDDO

        READ(26,"(A)") line_readin

        !one could search for the correct string or just hardcode it ...
        !searching might be more robust if it is written without format in VASP
        !RESULT = INDEX(line_readin, SUBSTRING [, BACK [, KIND]])

        !WRITE(6,*) TRIM(line_readin)
        !WRITE(6,*) line_readin(idx_start_fermi:idx_end_fermi)

        READ(line_readin(idx_start_fermi:idx_end_fermi),*,IOSTAT=ioerror) mu_dft
        IF ( ioerror .EQ. 0 ) THEN

	  IF ( rank_0 ) THEN
	    WRITE(6,"(3X,'-> Fermi-energy:',F10.6,1X,'eV')") mu_dft
	  ENDIF

        ELSE
          WRITE(6,"(3X,'Fermi-energy could not be read.')")
          WRITE(6,"(3X,'Please enter it manually:',1X)",ADVANCE='NO')
          READ(*,*) mu_dft
          WRITE(6,"(3X,'-> Fermi-energy:',1X,F10.6,1X,'eV')") mu_dft
        ENDIF

      ENDIF
    CLOSE(26)

    IF ( myrank .EQ. 0 ) THEN
      WRITE(6,*) '-'
    ENDIF

  END SUBROUTINE import_vasp_outcar_mu_dft

  !============================================================================================================================

  SUBROUTINE import_vasp_doscar_mu_dft

    IMPLICIT NONE

    ! Data dictionary: declare constants

    ! Data dictionary: declare variable types, definitions, units

    INTEGER :: ioerror !error code for file IO

    REAL :: tmp1, tmp2
    INTEGER :: tmp3

    !===========================================================================================================================

    OPEN (UNIT=26,FILE='./DOSCAR',STATUS='OLD', ACTION='READ', IOSTAT=ioerror)
    !Check for the correct OUTCAR file
      IF ( ioerror /= 0 ) THEN
        WRITE(6,"(3X,'DOSCAR file is missing.')")
        WRITE(6,"(3X,'Please enter Fermi-energy:',1X)",ADVANCE='NO')
        READ(*,*) mu_dft
        WRITE(6,"(3X,'-> Fermi-energy:',1X,F10.6,1X,'eV')") mu_dft

      ELSE
        IF ( rank_0 ) THEN
          WRITE(6,*) '  Reading in DOSCAR ...'
        ENDIF

        !skip until line six
        READ(26,*)
        READ(26,*)
        READ(26,*)
        READ(26,*)
        READ(26,*)
        READ(26,*,IOSTAT=ioerror) tmp1, tmp2, tmp3, mu_dft

        IF ( ioerror .EQ. 0 ) THEN

	  IF ( rank_0 ) THEN
	    WRITE(6,"(3X,'-> Fermi-energy:',F10.6,1X,'eV')") mu_dft
	  ENDIF

        ELSE
          WRITE(6,"(3X,'Fermi-energy could not be read.')")
          WRITE(6,"(3X,'Please enter it manually:',1X)",ADVANCE='NO')
          READ(*,*) mu_dft
          WRITE(6,"(3X,'-> Fermi-energy:',1X,F10.6,1X,'eV')") mu_dft
        ENDIF

      ENDIF
    CLOSE(26)

    IF ( myrank .EQ. 0 ) THEN
      WRITE(6,*) '-'
    ENDIF

  END SUBROUTINE import_vasp_doscar_mu_dft

END MODULE import
