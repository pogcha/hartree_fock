A simple hartree-fock solver

Reads in H(k) and Coulomb tensor and solves the Hartree-Fock equations iteratively. It can do magnetic calculations. Currently it is T=0.