CC = mpif90

#ifort
CFLAGS = -O2 -ipo9 -opt-prefetch -m64 -fp-model strict -real-size 64 -assume byterecl -assume bscc

#CFLAGS = -O2 -xHost -ipo9 -opt-prefetch -m64 -fp-model strict -real-size 64 -assume byterecl -assume bscc

#Production
#CFLAGS = -O3 -fast -axAVX -xhost -opt-prefetch -m64 -fp-model strict -integer-size 64 -real-size 64 -assume byterecl -assume bscc

#CFLAGS = -O2 -xHost -ipo9 -opt-prefetch -m64 -fp-model strict -real-size 64 -assume byterecl -assume bscc

#CFLAGS = -O2 -ipo9 -opt-prefetch -m64 -fp-model strict -real-size 64 -assume byterecl -assume bscc

#CFLAGS = -g -m64 -fp-model strict -real-size 64 -assume byterecl -assume bscc -check all -fpe0 -warn -traceback -debug extended

#HLRN: remove -integer-size 64 & use ftn

#Debug
#CFLAGS = -g -m64 -fp-model strict -real-size 64 -assume byterecl -assume bscc -check all -fpe0 -warn -traceback -debug extended

# Sequential link to MKL, prevents LAPACK/BLAS from beeing run with several threads, do not use in ITP, DGELLS seems bugged !
#LDFLAGS = -lmkl_intel_lp64 -lmkl_core -lmkl_sequential -lpthread -lm

LDFLAGS = -llapack -lblas -lpthread -lm

SOURCES = constants.f90 lib_math.f90 lib_parallel.f90 lib_vasplock.f90 timings.f90 lib_green.f90 shared_data.f90 lib_coulomb.f90 fileio.f90 import.f90 para_input.f90 main.f90

OBJECTS = $(SOURCES:.f90=.o)

EXECUTABLE = hartree_fock_1710.obelix

INSTALLPATH = ~/bin

all: build clean 

build: $(EXECUTABLE)

clean:
	rm -f *.o
	rm -f *.mod
	rm -f *~
	
install: 
	cp $(EXECUTABLE) $(INSTALLPATH)
	
.SUFFIXES: .o .f90
	
.f90.o:
	$(CC) $(CFLAGS) -c -o $@ $<
	
$(EXECUTABLE): $(OBJECTS)
	$(CC) -o $@ $(OBJECTS) $(LDFLAGS)
	
.PHONY: clean

.PHONY: install
