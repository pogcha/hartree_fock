MODULE constants
!
!  Purpose:
!    Declare parameters	
!
!  Record of revisions:
!      Date       Programmer	Description of change
!      ====       ==========	=====================
!    11/11/13     S. Barthel	Original code
!

IMPLICIT NONE

! Data dictionary: Declare constants in natural units (hbar=c=1, 4*pi*epsilon_0=1)

COMPLEX, PARAMETER :: IM = (0.0D0,1.0D0) 	!Imaginary unit
REAL, PARAMETER :: EPS = 1.0D-15		!decimal precision of double
!REAL, PARAMETER :: EPS = EPSILON(0.D0) 	!smallest number of double
!REAL, PARAMETER :: PI = 3.141592653589793 	!The number Pi
REAL, PARAMETER :: PI = 4*ATAN(1.0D0) 		!The number Pi (3.14159265358979)
REAL, PARAMETER :: kB = 8.6173324D-5 		!Boltzmann constant in eV/K
REAL, PARAMETER :: charge_e = 8.5424546D-2 	!Charge of electron
REAL, PARAMETER :: hbar = 1.0D0 		!Planck's constant
REAL, PARAMETER :: m_e = 510.99906D3 		!Mass of electron in eV

END MODULE constants
