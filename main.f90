PROGRAM main
!
!  Purpose:
!    Main program
!
!  Record of revisions:
!      Date       Programmer	Description of change
!      ====       ==========	=====================
!    27/04/15     S. Barthel	Original code
!

USE para_input
USE parallel
USE parallel
USE import
USE greens_functions
USE coulomb
USE vasplock

IMPLICIT NONE

! Data dictionary: declare constants

! Data dictionary: declare variable types, definitions, units
INTEGER :: allocerror,ioerror
INTEGER :: orbital, orbital_prime,orbital_2prime, spin, ion, kpoint
INTEGER :: iter
REAL, DIMENSION(:), ALLOCATABLE :: E_dc_amf
REAL, DIMENSION(:,:), ALLOCATABLE :: mean_density
REAL, DIMENSION(:,:,:), ALLOCATABLE :: orbital_density
INTEGER, dimension(2) :: spin_switch

CHARACTER(8)  :: date
CHARACTER(10) :: time
LOGICAL :: readSIGMAIN

INTEGER :: iw_read, spin_read, orbital__read, orbital_read, iw
REAL :: sigma_real_read, sigma_imag_read

spin_switch(1) = 1
spin_switch(2) = 2

!--------------------------------------------------------------------------------------------------------------------------------
!Initialize MPI variables
!------------------------------------------------------------------------------------------------------------------------------
CALL MPI_INIT(i_error)
CALL MPI_COMM_SIZE(MPI_COMM_WORLD, nprocs, i_error)
CALL MPI_COMM_RANK(MPI_COMM_WORLD, myrank, i_error)

CALL parallel_set_rank_0

CALL read_parameters

!--------------------------------------------------------------------------------------------------------------------------------
ALLOCATE(E_dc_amf(n_ions))

IF ( rank_0 ) THEN
  WRITE(6,*) 'lda+u hartree-fock solver 16.02, 02.02.2016'
  WRITE(6,*) 'developed by Stefan Barthel'
  WRITE(6,'(1X,"MPI: running on",1X,I3,1X,"cores ...")') nprocs
  CALL date_and_time(DATE=date)
  CALL date_and_time(TIME=time)
  WRITE(6,'(1X,A,".",A,".",A,"-",A,":",A,":",A)') date(1:4),date(5:6),date(7:8), time(1:2),time(3:4),time(5:6)
  WRITE(6,*) '-'
ENDIF

!only run if VASP542 is not running
CALL wait_no_vasplock

!-----------------------------------------------------------------------------------------------------------------------------------
!write some parameters of the run

IF ( rank_0 ) THEN
   IF ( T0 ) THEN
    WRITE(6,*) '*** Doing T=0 calculation'
  ELSE
    WRITE(6,*) '*** Doing finite temperature calculation'
  ENDIF
  WRITE(6,*) '*** Tolerance for selfconsistency, tol=',tol
  IF ( .NOT. use_sc_G ) THEN
    WRITE(6,*) '*** Non self-consistent G: Σ=G0V+Hartree'
  ELSE
    WRITE(6,*) '*** Self-consistent G: Σ=GV+Hartree'
  ENDIF

  IF ( use_para ) THEN
    WRITE(6,*) '*** Paramagnetic self-energy'
  ELSE
    WRITE(6,*) '*** Magnetic self-energy'
  ENDIF

  IF ( export_para ) THEN
    WRITE(6,*) '*** Export of paramagnetic self-energy'
  ELSE
    WRITE(6,*) '*** Export of magnetic self-energy'
  ENDIF

  IF ( use_t2g_eg_sym ) THEN
    WRITE(6,*) '*** Orbital density symmetrization : t2g/eg'
  ELSE
    WRITE(6,*) '*** No density symmetrization'
  ENDIF

  IF ( use_afm_sym ) THEN
    WRITE(6,*) '*** Antiferromagnetic symmetrization wrt. ions'
  ELSE
    WRITE(6,*) '*** No antiferromagnetic symmetrization'
  ENDIF

  IF ( use_sym_ions ) THEN
    WRITE(6,*) '*** Paramagnetic symmetrization wrt. ions'
  ELSE
    WRITE(6,*) '*** No Paramagnetic symmetrization'
  ENDIF

  IF ( read_coulomb ) THEN
    WRITE(6,*) '*** reading coulomb tensor from file UIJKL'
  ELSE
    WRITE(6,*) '*** constructing coulomb tensor'
  ENDIF

  IF ( set_mu_dc ) THEN
    WRITE(6,*) '*** using fixed double counting', mu_dc
  ELSE
    IF ( dc_FLL) THEN
        WRITE(6,*) '*** using FLL double counting'
    ELSEIF (dc_AMF) THEN
        WRITE(6,*) '*** using AMF double counting'
    ENDIF
  ENDIF

  IF ( is_metallic ) THEN
    WRITE(6,*) '*** Metallic µ-algorithm'
  ELSE
    WRITE(6,*) '*** Non-metallic µ-algorithm'
  ENDIF
  WRITE(6,'(3X,"Magnetic kick",1X,F21.16,1X,F21.16)') M
    WRITE(6,*) '*** Parameters for calculating DOS:'
    WRITE(6,'(3X,"number of points on real axis",1X,I5)') dos_N
    WRITE(6,'(3X,"calculating from... to...",1X,F21.16,1X,F21.16)') dos_min, dos_max
    WRITE(6,'(3X,"Magnetic quantum number L:",1X,I5)') L_ang

  WRITE(6,*) '-'
ENDIF

!-----------------------------------------------------------------------------------------------------------------------------------
!Load Hamiltonian
!-----------------------------------------------------------------------------------------------------------------------------------

CALL import_H_fBZ

!CALL import_HKcr_ctqmc			!do not use anymore

!CALL import_vasp_outcar_mu_dft		!only works for ISMEAR=-5
!CALL import_vasp_doscar_mu_dft
IF (set_mu_dft .EQV. .TRUE.) THEN
   mu_dft = mu_dftnew
ELSE
   mu_dft = 0.
ENDIF
!-----------------------------------------------------------------------------------------------------------------------------------
!Construct Coulomb-interaction matrix
!-----------------------------------------------------------------------------------------------------------------------------------

IF (read_coulomb .EQV. .TRUE.) THEN
   CALL read_coulombtensor()
ELSE
   CALL construct_coulombtensor(U,J)
ENDIF


!DO orbital=1,L_max
!    DO orbital_prime = 1,L_max
!    DO spin=1,L_max
!      DO ion=1,L_max
!
!
!                WRITE(6,*) orbital,orbital_prime,spin,ion, real(Uijkl_cubic(orbital,orbital_prime,spin,ion)), imag(Uijkl_cubic(orbital,orbital_prime,spin,ion))
!            ENDDO
!        ENDDO
!      ENDDO
!    ENDDO

!-----------------------------------------------------------------------------------------------------------------------------------
!Initialization of Matsubara Greens-function
!-----------------------------------------------------------------------------------------------------------------------------------
CALL gf_iw_ALLOCATE( G_HKcf, n_iw, n_spins, n_kpoints, n_bands_cr)

IF (T0) THEN
    CALL gf_iw_SET_MESH_T0( G_HKcf, beta, 40 )
ELSE
    CALL gf_iw_SET_MESH( G_HKcf, beta, fit_start )
END IF


CALL gf_iw_SET_H0( G_HKcf, H0, k_weight )



!-----------------------------------------------------------------------------------------------------------------------------------
!Calculate G0
!-----------------------------------------------------------------------------------------------------------------------------------
ALLOCATE(mean_density(n_ions,n_spins))
ALLOCATE(orbital_density(n_ions,L_max,n_spins))
ALLOCATE ( S_HF(n_bands_cr, n_bands_cr, 2, n_iw), S_HF_old(n_bands_cr, n_bands_cr, 2, n_iw), STAT=allocerror )
IF ( allocerror > 0 ) THEN
    WRITE(6,*) 'ERROR: Memory for "S_HF" could not be allocated'
    STOP
ELSE
    S_HF = CMPLX(0.,0.)
    S_HF_old = CMPLX(0.,0.)
ENDIF

CALL gf_iw_SET_SIGMA_ZERO( G_HKcf )

! changed from _EIGEN to _DIAG so we also calculate the eigenvectors
IF ( T0 ) THEN
    CALL gf_iw_CALC_DIAG( G_HKcf )
    CALL gf_iw_CALC_DIAG0( G_HKcf )
ELSE
    IF ( rank_0 ) THEN
        WRITE(6,*) 'NOT calling EIGEN!'
    END IF
    !CALL gf_iw_CALC_EIGEN( G_HKcf )
ENDIF


CALL gf_iw_SET_NE( G_HKcf, REAL(n_electrons) )

CALL gf_iw_SET_MU( G_HKcf, mu_dft )

!-----------------------------------------------------------------------------------------------------------------------------------

! !SrVO3
! mu_int(1) = 0.
! mu_int(2) = mu_dft
! mu_int(3) = 20.

! !NiO
! mu_int(1) = 0.
! mu_int(2) = mu_dft
! mu_int(3) = 20.

! !ZnO
! mu_int(1) = 0.
! mu_int(2) = mu_dft
! mu_int(3) = 2.

! !Cr
! mu_int(1) = 0.
! mu_int(2) = mu_dft
! mu_int(3) = 10.

!-----------------------------------------------------------------------------------------------------------------------------------
mu_int(2) = mu_dft


IF ( T0 ) THEN
    CALL gf_iw_ADJUST_MU_T0( G_HKcf, mu_int, is_metallic, .TRUE., set_mu_dft)	!offdiagonals included
ELSE
    CALL gf_iw_ADJUST_MU( G_HKcf, mu_int, is_metallic, .TRUE., set_mu_dft)	!offdiagonals included
ENDIF




!CALL EXIT(0)
iter = 0
mu_old = 0.
mu = gf_iw_RETURN_MU( G_HKcf )
IF (set_mu_dft .EQV. .TRUE.) THEN
   mu=mu_int(2)
   delta_mu = mu-mu_old
ELSE
   delta_mu = mu-mu_old
   mu_int(2) = mu
ENDIF

IF ( rank_0 ) THEN
  OPEN (UNIT=26,FILE='./MU.TXT',STATUS='REPLACE', ACTION='WRITE', IOSTAT=ioerror)
  WRITE(26,'(3X,I3,3X,F19.16,1X,F19.16,1X,F19.16)') iter, mu_dft, mu, ABS(delta_mu)
  CLOSE(26)
ENDIF

!-----------------------------------------------------------------------------------------------------------------------------------

Epot_old = 0.
Epot = 0.
delta_Epot = Epot-Epot_old

IF ( rank_0 ) THEN
  OPEN (UNIT=26,FILE='./EPOT.TXT',STATUS='REPLACE', ACTION='WRITE', IOSTAT=ioerror)
  WRITE(26,'(3X,I3,3X,F21.16,1X,F19.16)') iter, Epot, ABS(delta_Epot)
  CLOSE(26)
ENDIF

!-----------------------------------------------------------------------------------------------------------------------------------

ALLOCATE ( density_G(n_bands_cr, n_bands_cr, 2), STAT=allocerror )
ALLOCATE ( density_G0(n_bands_cr, n_bands_cr, 2), STAT=allocerror )
IF ( allocerror > 0 ) THEN
    WRITE(6,*) 'ERROR: Memory for "density_G" could not be allocated'
    STOP
ELSE

  density_G = gf_iw_RETURN_DENSITY( G_HKcf, use_t2g_eg_sym, use_afm_sym, use_sym_ions, n_ions )

  IF ( rank_0 ) THEN
    OPEN (UNIT=26,FILE='./DENSITY_G0_UP.TXT',STATUS='REPLACE', ACTION='WRITE', IOSTAT=ioerror)
    DO orbital=1,L_max*n_ions
      DO orbital_prime=1,L_max*n_ions
	WRITE(26,'(3X,I2,1X,I2,3X,F19.16,1X,F19.16)') orbital, orbital_prime, density_G(orbital,orbital_prime,1)
      ENDDO
    ENDDO
    CLOSE(26)
  ENDIF

  IF ( rank_0 ) THEN
    OPEN (UNIT=26,FILE='./DENSITY_G0_DN.TXT',STATUS='REPLACE', ACTION='WRITE', IOSTAT=ioerror)
    DO orbital=1,L_max*n_ions
      DO orbital_prime=1,L_max*n_ions
	WRITE(26,'(3X,I2,1X,I2,3X,F19.16,1X,F19.16)') orbital, orbital_prime, density_G(orbital,orbital_prime,2)
      ENDDO
    ENDDO
    CLOSE(26)
  ENDIF

ENDIF

!Total number of electrons in each spin-channel for each ions
N_tot = 0.
DO ion=1,n_ions
  DO orbital=1,L_max
    N_tot(ion,:) = N_tot(ion,:) + REAL( density_G( (ion-1)*L_max+orbital, (ion-1)*L_max+orbital,:) )
  ENDDO
ENDDO

!keep initial LDA occupancies for Double-counting correction
N_tot_0 = N_tot
density_G0 = density_G


IF ( rank_0 ) THEN
    DO ion=1,n_ions
      WRITE(6,"(3X,'Ion :',1X,I2,1X)") ion
      WRITE(6,*) '  N(d,↑) (e-): ', N_tot(ion,1)
      WRITE(6,*) '  N(d,↓) (e-): ', N_tot(ion,2)
      WRITE(6,*) '  d-Magnetic moment (µB): ', N_tot(ion,1)-N_tot(ion,2)
      WRITE(6,*) '  N(d) (e-): ', SUM(N_tot(ion,:))
      WRITE(6,*) '-'
    ENDDO
ENDIF

!-----------------------------------------------------------------------------------------------------------------------------------

!calculate and export dos
IF ( rank_0 ) THEN
  WRITE(6,*) '  Calculating and exporting G0 ...'
  WRITE(6,*) '-'
ENDIF
IF ( T0 ) THEN
    CALL gf_iw_CALC_EXPORT_DOS_HF_T0( G_HKcf,'G0  ',idelta,dos_N,dos_min,dos_max)
ELSE
    CALL gf_iw_CALC_EXPORT_DOS_HF( G_HKcf,'G0  ',idelta)
ENDIF
IF ( calc_G0_dos_only ) THEN

  IF ( rank_0 ) THEN
    WRITE(6,*) 'Waiting for MPI tasks to finish ...'
  ENDIF

  CALL MPI_Barrier(MPI_COMM_WORLD, i_error)
  IF ( rank_0 ) THEN
    WRITE(6,*) 'Early exit.'
  ENDIF

  CALL MPI_FINALIZE(i_error)
  STOP

ENDIF

!-----------------------------------------------------------------------------------------------------------------------------------

!use self-consistent G flag
IF ( .NOT. use_sc_G ) THEN
  n_iter = 1
ENDIF

DO WHILE ( ( (ABS(delta_mu) .GT. tol) .OR. (ABS(delta_Epot) .GT. tol) ) .AND. (iter < n_iter) )

  iter=iter+1

  IF ( rank_0 ) THEN
    WRITE(6,*) 'Iteration: ', iter
    WRITE(6,*)
  ENDIF

  !---------------------------------------------------------------------------------------------------------------------------------
  !Double-counting using initial LDA occupancies
  !---------------------------------------------------------------------------------------------------------------------------------

  !FLL double-counting energy correction: Eq.(4) of PRB 52, 8 R5467, (1995), Lichtenstein, Anisimov, Zaanen
  WRITE(6,*) 'N_tot_0', N_tot_0
  IF (set_mu_dc .EQV. .FALSE.) THEN
     DO ion=1,n_ions
       !initial LDA
       E_dc_fll(ion) = 0.5*U*SUM(N_tot_0(ion,:))*(SUM(N_tot_0(ion,:))-1.0) &
         - 0.5*J*( N_tot_0(ion,1)*(N_tot_0(ion,1)-1.0) &
         + N_tot_0(ion,2)*(N_tot_0(ion,2)-1.0) )
!        !dynamic HF
!        E_dc_fll(ion) = 0.5*U*SUM(N_tot(ion,:))*(SUM(N_tot(ion,:))-1.0) &
!          - 0.5*J*( N_tot(ion,1)*(N_tot(ion,1)-1.0) &
!          + N_tot(ion,2)*(N_tot(ion,2)-1.0) )
     ENDDO

     mean_density = 0
     orbital_density = 0

     DO spin=1,n_spins
        DO ion=1,n_ions
           DO orbital=1,L_max
              mean_density(ion, spin) = mean_density(ion, spin)+ REAL( density_G0( (ion-1)*L_max+orbital, (ion-1)*L_max+orbital,spin) )/L_max
              orbital_density(ion,orbital,spin) = orbital_density(ion,orbital,spin) + REAL( density_G0( (ion-1)*L_max+orbital, (ion-1)*L_max+orbital,spin))
           ENDDO
        ENDDO
     ENDDO

     DO ion=1,n_ions
        E_dc_amf(ion) = 0.5*U*(SUM(N_tot_0(ion,:))-SUM(mean_density(ion,:)))*(SUM(N_tot_0(ion,:))-SUM(mean_density(ion,:))) &
         + 0.5*(U-J)*(SUM(N_tot_0(ion,:))-SUM(mean_density(ion,:)))*(SUM(N_tot_0(ion,:))-SUM(mean_density(ion,:))) &
         - 0.5*(U-J)*SUM((orbital_density(ion,:,:)-SUM(mean_density(ion,:)))**2)
     ENDDO

     IF ( rank_0 ) THEN
       WRITE(6,"(3X,'FLL Double-counting')")
       DO ion=1,n_ions
         WRITE(6,"(3X,'Ion :',1X,I2,1X)") ion
         WRITE(6,"(3X,'Energy    E_dc_fll :',1X,F11.7,1X,'eV')") E_dc_fll(ion)
         WRITE(6,"(3X,'Potential V(↑) :',1X,F11.7,1X,'eV')") U*(SUM(N_tot_0(ion,:))-0.5)-J*(N_tot_0(ion,1)-0.5)
         WRITE(6,"(3X,'Potential V(↓) :',1X,F11.7,1X,'eV')") U*(SUM(N_tot_0(ion,:))-0.5)-J*(N_tot_0(ion,2)-0.5)
         WRITE(6,*)
       ENDDO
       WRITE(6,"(3X,'AMF Double-counting')")
       DO ion=1,n_ions
         WRITE(6,"(3X,'Ion :',1X,I2,1X)") ion
         WRITE(6,"(3X,'Energy    E_dc_amf :',1X,F11.7,1X,'eV')") E_dc_amf(ion)
         WRITE(6,"(3X,'Potential V(↑) :',1X,F11.7,1X,'eV')") U*(SUM(N_tot_0(ion,:))-0.5)-J*(N_tot_0(ion,1)-0.5)-(U-J)*(mean_density(ion,1)-0.5)
         WRITE(6,"(3X,'Potential V(↓) :',1X,F11.7,1X,'eV')") U*(SUM(N_tot_0(ion,:))-0.5)-J*(N_tot_0(ion,2)-0.5)-(U-J)*(mean_density(ion,2)-0.5)
         !WRITE(6,"(3X,'Potential V(↑) :',1X,F11.7,1X,'eV')") U*(SUM(N_tot_0(ion,:))-SUM(mean_density(ion,:)))-J*(N_tot_0(ion,1)-SUM(mean_density(ion,:)))
         !WRITE(6,"(3X,'Potential V(↓) :',1X,F11.7,1X,'eV')") U*(SUM(N_tot_0(ion,:))-SUM(mean_density(ion,:)))-J*(N_tot_0(ion,2)-SUM(mean_density(ion,:)))
         !WRITE(6,"(3X,'Potential V(↑) :',1X,F11.7,1X,'eV')") U*(SUM(N_tot_0(ion,:))-SUM(mean_density(ion,:)))-(U-J)*(N_tot_0(ion,1)-SUM(mean_density(ion,:)))
         !WRITE(6,"(3X,'Potential V(↓) :',1X,F11.7,1X,'eV')") U*(SUM(N_tot_0(ion,:))-SUM(mean_density(ion,:)))-(U-J)*(N_tot_0(ion,2)-SUM(mean_density(ion,:)))
         WRITE(6,*)
       ENDDO
     ENDIF
  ELSE
    DO ion=1,n_ions
       !initial LDA
       E_dc_fll(ion) = 0.5*U*SUM(N_tot_0(ion,:))*(SUM(N_tot_0(ion,:))-1.0) &
         - 0.5*J*( N_tot_0(ion,1)*(N_tot_0(ion,1)-1.0) &
         + N_tot_0(ion,2)*(N_tot_0(ion,2)-1.0) )
!        !dynamic HF
!        E_dc_fll(ion) = 0.5*U*SUM(N_tot(ion,:))*(SUM(N_tot(ion,:))-1.0) &
!          - 0.5*J*( N_tot(ion,1)*(N_tot(ion,1)-1.0) &
!          + N_tot(ion,2)*(N_tot(ion,2)-1.0) )
     ENDDO

     mean_density = 0
     orbital_density = 0

     DO spin=1,n_spins
        DO ion=1,n_ions
           DO orbital=1,L_max
              mean_density(ion, spin) = mean_density(ion, spin)+ REAL( density_G0( (ion-1)*L_max+orbital, (ion-1)*L_max+orbital,spin) )/L_max
              orbital_density(ion,orbital,spin) = orbital_density(ion,orbital,spin) + REAL( density_G0( (ion-1)*L_max+orbital, (ion-1)*L_max+orbital,spin))
           ENDDO
        ENDDO
     ENDDO

     DO ion=1,n_ions
        E_dc_amf(ion) = 0.5*U*(SUM(N_tot_0(ion,:))-SUM(mean_density(ion,:)))*(SUM(N_tot_0(ion,:))-SUM(mean_density(ion,:))) &
         + 0.5*(U-J)*(SUM(N_tot_0(ion,:))-SUM(mean_density(ion,:)))*(SUM(N_tot_0(ion,:))-SUM(mean_density(ion,:))) &
         - 0.5*(U-J)*SUM((orbital_density(ion,:,:)-SUM(mean_density(ion,:)))**2)
     ENDDO

     IF ( rank_0 ) THEN
       WRITE(6,"(3X,'FLL Double-counting')")
       DO ion=1,n_ions
         WRITE(6,"(3X,'Ion :',1X,I2,1X)") ion
         WRITE(6,"(3X,'Energy    E_dc_fll :',1X,F11.7,1X,'eV')") E_dc_fll(ion)
         WRITE(6,"(3X,'Potential V(↑) :',1X,F11.7,1X,'eV')") U*(SUM(N_tot_0(ion,:))-0.5)-J*(N_tot_0(ion,1)-0.5)
         WRITE(6,"(3X,'Potential V(↓) :',1X,F11.7,1X,'eV')") U*(SUM(N_tot_0(ion,:))-0.5)-J*(N_tot_0(ion,2)-0.5)
         WRITE(6,*)
       ENDDO
       WRITE(6,"(3X,'AMF Double-counting')")
       DO ion=1,n_ions
         WRITE(6,"(3X,'Ion :',1X,I2,1X)") ion
         WRITE(6,"(3X,'Energy    E_dc_amf :',1X,F11.7,1X,'eV')") E_dc_amf(ion)
         WRITE(6,"(3X,'Potential V(↑) :',1X,F11.7,1X,'eV')") U*(SUM(N_tot_0(ion,:))-0.5)-J*(N_tot_0(ion,1)-0.5)-(U-J)*(mean_density(ion,1)-0.5)
         WRITE(6,"(3X,'Potential V(↓) :',1X,F11.7,1X,'eV')") U*(SUM(N_tot_0(ion,:))-0.5)-J*(N_tot_0(ion,2)-0.5)-(U-J)*(mean_density(ion,2)-0.5)
         !WRITE(6,"(3X,'Potential V(↑) :',1X,F11.7,1X,'eV')") U*(SUM(N_tot_0(ion,:))-SUM(mean_density(ion,:)))-J*(N_tot_0(ion,1)-SUM(mean_density(ion,:)))
         !WRITE(6,"(3X,'Potential V(↓) :',1X,F11.7,1X,'eV')") U*(SUM(N_tot_0(ion,:))-SUM(mean_density(ion,:)))-J*(N_tot_0(ion,2)-SUM(mean_density(ion,:)))
         WRITE(6,*)
       ENDDO
     ENDIF
     IF ( rank_0 ) THEN
        WRITE(6,"(3X,'µ_DC has been fixed')")
        WRITE(6,"(3X,'Potential V(↑) :',1X,F11.7,1X,'eV')") mu_dc
        WRITE(6,"(3X,'Potential V(↓) :',1X,F11.7,1X,'eV')") mu_dc
     ENDIF
  ENDIF


  !---------------------------------------------------------------------------------------------------------------------------------
  !Hartree-Fock Self-energy for spin-diagonal G0 & density-matrix
  !Less simplified version of Eq.(5) of PRB 52, 8 R5467, (1995), Lichtenstein, Anisimov, Zaanen
  !---------------------------------------------------------------------------------------------------------------------------------
  S_HF_old = S_HF		!copy for later mixing
  S_HF = CMPLX(0.,0.)

  !magnetic calculation (initial magnetic energy)
  IF ( (.NOT. use_para) .AND. iter == 1 ) THEN
    DO spin=1,n_spins
      DO ion=1,n_ions
	DO orbital=1,L_max
	  S_HF((ion-1)*L_max+orbital,(ion-1)*L_max+orbital,spin,:) = M(ion)*(-1.0)**(spin-1)
	ENDDO
      ENDDO
    ENDDO
  ENDIF
    IF ( .TRUE. ) THEN

    WRITE(6,*) spin_switch
  DO spin=1,n_spins
    DO ion=1,n_ions
      DO orbital=1,L_max
	DO orbital_prime=1,L_max
	  S_HF((ion-1)*L_max+orbital,(ion-1)*L_max+orbital_prime,spin,:) = &
	    S_HF((ion-1)*L_max+orbital,(ion-1)*L_max+orbital_prime,spin,:) + &
	    0.5 *(SUM( (Uijkl_cubic(orbital,:,:,orbital_prime) + Uijkl_cubic(:,orbital,orbital_prime,:)) &
		* SUM(density_G((ion-1)*L_max+1:(ion-1)*L_max+L_max,(ion-1)*L_max+1:(ion-1)*L_max+L_max,:),3) ) &
		- SUM( (Uijkl_cubic(:,orbital,:,orbital_prime) + Uijkl_cubic(orbital,:,orbital_prime,:)) &
		* density_G((ion-1)*L_max+1:(ion-1)*L_max+L_max,(ion-1)*L_max+1:(ion-1)*L_max+L_max,spin_switch(spin)) ) )
	ENDDO
      ENDDO
    ENDDO
  ENDDO

  !FLL double-counting correction: Eq.(5) of PRB 52, 8 R5467, (1995), Lichtenstein, Anisimov, Zaanen
  DO spin=1,n_spins
    DO ion=1,n_ions
      DO orbital=1,L_max
        !initial LDA
        IF (set_mu_dc .EQV. .TRUE.) THEN
            S_HF((ion-1)*L_max+orbital,(ion-1)*L_max+orbital,spin,:) = S_HF((ion-1)*L_max+orbital,(ion-1)*L_max+orbital,spin,:) &
            - mu_dc
        ELSE
            IF (dc_FLL) THEN
                S_HF((ion-1)*L_max+orbital,(ion-1)*L_max+orbital,spin,:) = S_HF((ion-1)*L_max+orbital,(ion-1)*L_max+orbital,spin,:) &
                - (U*(SUM(N_tot_0(ion,:))-0.5)-J*(N_tot_0(ion,spin)-0.5) )
            ELSEIF (dc_AMF) then
                S_HF((ion-1)*L_max+orbital,(ion-1)*L_max+orbital,spin,:) = S_HF((ion-1)*L_max+orbital,(ion-1)*L_max+orbital,spin,:) &
                -U*(SUM(N_tot_0(ion,:))-0.5)-J*(N_tot_0(ion,spin)-0.5)-(U-J)*(mean_density(ion,1)-0.5)
            ENDIF
        ENDIF


! 	!dynamic HF
! 	S_HF((ion-1)*L_max+orbital,(ion-1)*L_max+orbital,spin,:) = S_HF((ion-1)*L_max+orbital,(ion-1)*L_max+orbital,spin,:) &
! 	  - ( U*(SUM(N_tot(ion,:))-0.5)-J*(N_tot(ion,spin)-0.5) )

	!dynamic HF
	!S_HF(orbital,orbital,spin,:) = S_HF(orbital,orbital,spin,:) - ( U*(SUM(N_tot)-0.5)-J*(N_tot(spin)-0.5) )

      ENDDO
    ENDDO
  ENDDO
    ENDIF
  !read starting sigma from file
  IF ( read_sigma .AND. iter == 1 ) THEN
    readSIGMAIN = .TRUE.
    IF ( rank_0 ) THEN

      !read starting sigma from file
      !OPEN (UNIT=26,FILE='SIGMA.IN',STATUS='UNKNOWN', ACTION='READ', IOSTAT=ioerror)
      IF ( readSIGMAIN ) THEN
        OPEN (UNIT=26,FILE='./SIGMA.IN',STATUS='OLD', ACTION='READ', IOSTAT=ioerror)
      ELSE
        OPEN (UNIT=26,FILE='./SIGMA.DAT',STATUS='OLD', ACTION='READ', IOSTAT=ioerror)
      ENDIF
      IF ( ioerror /= 0 ) THEN
	WRITE(6,"(3X,'Initial self-energy by G0-density or by magnetic kick')")
      ELSE
	WRITE(6,"(3X,'Initial self-energy from file (possibly old Double-counting)')")


    !header
    S_HF = CMPLX(0.,0.)	!reset

    IF ( readSIGMAIN ) THEN
        WRITE(6,"(3X,'Reading from SIGMA.IN!')")
        DO spin=1,n_spins
          DO ion=1,n_ions
            DO orbital=1,L_max
              DO orbital_prime=1,L_max
        	READ(26,*) S_HF((ion-1)*L_max+orbital,(ion-1)*L_max+orbital_prime,spin,1)
        	S_HF((ion-1)*L_max+orbital,(ion-1)*L_max+orbital_prime,spin,:) = &
        	  S_HF((ion-1)*L_max+orbital,(ion-1)*L_max+orbital_prime,spin,1)
              ENDDO
            ENDDO
          ENDDO
        ENDDO
    ELSE
        WRITE(6,"(3X,'Reading from SIGMA.DAT instead from SIGMA.IN!')")
        READ(26,*)
        READ(26,*)
        READ(26,*)
        READ(26,*)
        READ(26,*)
        READ(26,*)
        READ(26,*)
        READ(26,*)

        !data
          DO iw=1,n_iw
            DO spin=1,n_spins
                DO orbital_prime=1,n_bands_cr
                    DO orbital=1,n_bands_cr
                        READ(26,*) orbital_read, orbital__read, spin_read, iw_read
                        READ(26,*) sigma_real_read, sigma_imag_read
                        !IF ( rank_0 ) THEN
                        !    WRITE(6,*) orbital_read, orbital__read, spin_read, iw_read,sigma_real_read, sigma_imag_read
                        !END IF
                        S_HF(orbital_read, orbital__read, spin_read, iw_read) = CMPLX(sigma_real_read, sigma_imag_read)
                    ENDDO
                ENDDO
            ENDDO

          ENDDO
    ENDIF
          ENDIF
      CLOSE(26)

      WRITE(6,*)

    ENDIF

    !broadcast self-energy
    srcnt = n_bands_cr*n_bands_cr*2*n_iw
    CALL MPI_BCAST(S_HF,  srcnt, MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD, i_error)
    CALL check_mpi_success

  ENDIF
  !---------------------------------------------------------------------------------------------------------------------------------

  !paramagnetic calculation (symmetrization wrt. spin)
  IF ( use_para ) THEN
    S_HF(:,:,1,:) = SUM(S_HF,3)/2
    S_HF(:,:,2,:) = S_HF(:,:,1,:)
  ENDIF
  !---------------------------------------------------------------------------------------------------------------------------------

  !Linear mixing, do not mix in first step to avoid mixing a zero self-energy, corrected 21.01.16
  IF ( iter > 1 ) THEN
    S_HF = mix_new_sigma * S_HF + (1.-mix_new_sigma) * S_HF_old
    IF ( rank_0 ) THEN
      WRITE(6,*) '  Mixing factor for new self-energy: ', mix_new_sigma
      WRITE(6,*)
    ENDIF
  ENDIF
  IF ( rank_0 ) THEN
    OPEN (UNIT=26,FILE='SIGMA.TXT',STATUS='REPLACE', ACTION='WRITE', IOSTAT=ioerror)
    DO orbital=1,L_max*n_ions
      DO orbital_prime=1,L_max*n_ions
	WRITE(26,'(3X,I2,1X,I2,3X,F19.16,1X,F19.16)') orbital, orbital_prime, S_HF(orbital,orbital_prime,1,1)
      ENDDO
    ENDDO
    CLOSE(26)
    !write starting sigma to file for next call
    OPEN (UNIT=26,FILE='SIGMA.IN',STATUS='REPLACE', ACTION='WRITE', IOSTAT=ioerror)
    DO spin=1,n_spins
      DO ion=1,n_ions
	DO orbital=1,L_max
	  DO orbital_prime=1,L_max
	    WRITE(26,*) S_HF((ion-1)*L_max+orbital,(ion-1)*L_max+orbital_prime,spin,1)
	  ENDDO
	ENDDO
      ENDDO
    ENDDO
    CLOSE(26)
  ENDIF

  !---------------------------------------------------------------------------------------------------------------------------------

  IF ( T0 ) THEN
    CALL gf_iw_SET_SIGMA_T0( G_HKcf, S_HF )
  ELSE
    CALL gf_iw_SET_SIGMA( G_HKcf, S_HF )
  ENDIF

  IF ( T0 ) THEN
    CALL gf_iw_CALC_DIAG( G_HKcf )
  ELSE
    IF ( rank_0 ) THEN
        WRITE(6,*) 'NOT callling EIGEN!'
    END IF
    !CALL gf_iw_CALC_EIGEN( G_HKcf )
  ENDIF

  IF ( T0 ) THEN
     CALL gf_iw_ADJUST_MU_T0( G_HKcf, mu_int, is_metallic, .FALSE., set_mu_dft)	!offdiagonals included
  ELSE
     CALL gf_iw_ADJUST_MU( G_HKcf, mu_int, is_metallic, .FALSE., set_mu_dft)	!offdiagonals included
  ENDIF

  IF (set_mu_dft .EQV. .FALSE.) THEN
     mu_old = mu
     mu = gf_iw_RETURN_MU( G_HKcf )
     delta_mu = mu-mu_old

     !new mu intervall (linear extrapolated)
     mu_int(1) = mu-20*ABS(delta_mu)
     mu_int(2) = mu
     mu_int(3) = mu+20*ABS(delta_mu)

!      !new mu intervall
!      mu_int(1) = mu-1.0
!      mu_int(2) = mu
!      mu_int(3) = mu+1.0
  ENDIF
  !export
  IF ( rank_0 ) THEN
    OPEN (UNIT=26,FILE='./MU.TXT',STATUS='OLD', ACTION='WRITE', ACCESS='APPEND', IOSTAT=ioerror)
    WRITE(26,'(3X,I3,3X,F19.16,1X,F19.16,1X,F19.16)') iter, mu_dft, mu, ABS(delta_mu)
    CLOSE(26)
  ENDIF

  !---------------------------------------------------------------------------------------------------------------------------------


  Epot_old = Epot
  IF ( T0 ) THEN
    Epot = gf_iw_CALC_Epot_T0( G_HKcf )
    E_kin = gf_iw_CALC_Ekin_T0( G_HKcf )
  ELSE
    Epot = gf_iw_CALC_Epot( G_HKcf )
    ! kinetic energy for T=0 not implemented
    E_kin = 0.0
  ENDIF
  delta_Epot = Epot-Epot_old


  IF ( rank_0 ) THEN
    WRITE(6,"(3X,'Potential energy arising from beyond-DFT interactions')")
    WRITE(6,"(3X,'Energy    Epot :',1X,F11.7,1X,'eV')") Epot
    WRITE(6,*)
  ENDIF

  IF ( rank_0 ) THEN
    OPEN (UNIT=26,FILE='./EPOT.TXT',STATUS='OLD', ACTION='WRITE', ACCESS='APPEND', IOSTAT=ioerror)
    WRITE(26,'(3X,I3,3X,F21.16,1X,F19.16)') iter, Epot, ABS(delta_Epot)
    CLOSE(26)
  ENDIF

  !---------------------------------------------------------------------------------------------------------------------------------

  density_G = gf_iw_RETURN_DENSITY( G_HKcf, use_t2g_eg_sym, use_afm_sym, use_sym_ions, n_ions )


IF ( rank_0 ) THEN
    OPEN (UNIT=26,FILE='./DENSITY_UP.TXT',STATUS='REPLACE', ACTION='WRITE', IOSTAT=ioerror)
    DO orbital=1,L_max*n_ions
      DO orbital_prime=1,L_max*n_ions
	WRITE(26,'(3X,I2,1X,I2,3X,F19.16,1X,F19.16)') orbital, orbital_prime, density_G(orbital,orbital_prime,1)
      ENDDO
    ENDDO
    CLOSE(26)
  ENDIF

  IF ( rank_0 ) THEN
    OPEN (UNIT=26,FILE='./DENSITY_DN.TXT',STATUS='REPLACE', ACTION='WRITE', IOSTAT=ioerror)
    DO orbital=1,L_max*n_ions
      DO orbital_prime=1,L_max*n_ions
	WRITE(26,'(3X,I2,1X,I2,3X,F19.16,1X,F19.16)') orbital, orbital_prime, density_G(orbital,orbital_prime,2)
      ENDDO
    ENDDO
    CLOSE(26)
  ENDIF






 IF ( rank_0 ) THEN
    WRITE(6,"(3X,'energy')")
    WRITE(6,"(3X,'Energy    Ekin :',1X,F11.7,1X,'eV')") E_kin
    WRITE(6,"(3X,'Energy    Epot :',1X,F11.7,1X,'eV')") Epot
    WRITE(6,"(3X,'Energy    Etot :',1X,F11.7,1X,'eV')") Epot+E_kin
    WRITE(6,*)
  ENDIF

  !Total number of electrons in each spin-channel for each ion
  N_tot = 0.
  DO ion=1,n_ions
    DO orbital=1,L_max
      N_tot(ion,:) = N_tot(ion,:) + REAL( density_G( (ion-1)*L_max+orbital, (ion-1)*L_max+orbital,:) )
    ENDDO
  ENDDO

  !IF ( rank_0 ) THEN
  !  WRITE(6,"(3X,'WARNING doing self-consisent dc!')")
  !ENDIF

  !N_tot_0(:,1) = 0.5*(N_tot(:,1)  + N_tot(:,2) )
  !N_tot_0(:,2) = 0.5*(N_tot(:,1)  + N_tot(:,2) )

  IF ( rank_0 ) THEN
    DO ion=1,n_ions
      WRITE(6,"(3X,'Ion :',1X,I2,1X)") ion
      WRITE(6,*) '  N(d,↑) (e-): ', N_tot(ion,1)
      WRITE(6,*) '  N(d,↓) (e-): ', N_tot(ion,2)
      WRITE(6,*) '  d-Magnetic moment (µB): ', N_tot(ion,1)-N_tot(ion,2)
      WRITE(6,*) '  N(d) (e-): ', SUM(N_tot(ion,:))
      WRITE(6,*)
    ENDDO
  ENDIF

  !-----------------------------------------------------------------------------------------------------------------------------------

  !convergence
  IF ( rank_0 ) THEN
    WRITE(6,*) '  Delta-µ: ', delta_mu
    WRITE(6,*) '  Delta-Epot: ', delta_Epot
    WRITE(6,*)
  ENDIF

ENDDO

IF (rank_0) then
    WRITE(6,*) 'Ending HF-cycle:'
    IF ( ( ABS(delta_mu) .GT. tol) .OR. (ABS(delta_Epot) .GT. tol) ) then
        WRITE(6,*) '  Maximum number of iterations reached.\n'
    else
        WRITE(6,*) '  Tolerance of chemical potential or potential energy reached.\n'
    END IF
END IF


!-----------------------------------------------------------------------------------------------------------------------------------

!calculate and export dos
IF ( rank_0 ) THEN
  WRITE(6,*) '  Calculating and exporting G ...'
  WRITE(6,*) '-'
ENDIF

IF ( T0 ) THEN
    CALL gf_iw_CALC_EXPORT_DOS_HF_T0( G_HKcf,'G   ',idelta,dos_N,dos_min,dos_max)
ELSE
    CALL gf_iw_CALC_EXPORT_DOS_HF( G_HKcf,'G   ',idelta)
ENDIF
!export final self-energy, possibly paramagnetic for VASP
IF ( rank_0 ) THEN
  WRITE(6,*) '  Calculating and exporting S ...'
  WRITE(6,*) '-'
ENDIF
CALL gf_iw_EXPORT_SIGMA( G_HKcf, export_para )

!-----------------------------------------------------------------------------------------------------------------------------------

!export chemical potentials during scf-loop with VASP
IF ( rank_0 ) THEN
  OPEN (UNIT=26,FILE='./MU_HF.TXT',STATUS='OLD', ACTION='WRITE', ACCESS='APPEND', IOSTAT=ioerror)
  IF ( ioerror /= 0 ) THEN
    OPEN (UNIT=26,FILE='./MU_HF.TXT',STATUS='NEW', ACTION='WRITE', IOSTAT=ioerror)
  ENDIF
  WRITE(26,'(3X,F19.16,1X,F22.16)') mu, Epot
  CLOSE(26)
ENDIF

!-----------------------------------------------------------------------------------------------------------------------------------
!Deallocation
!-----------------------------------------------------------------------------------------------------------------------------------
IF ( rank_0 ) THEN
  WRITE(6,*) 'Deallocating ...'
ENDIF

DEALLOCATE ( H0, STAT=allocerror )
IF ( allocerror > 0 ) THEN
  WRITE(*,*) 'ERROR: Memory for "H0" could not be deallocated'
  STOP
ENDIF

DEALLOCATE ( Uijkl_spherical, Uijkl_cubic, STAT=allocerror )
IF ( allocerror > 0 ) THEN
  WRITE(*,*) 'ERROR: Memory for "Uijkl_spherical, Uijkl_cubic" could not be deallocated'
  STOP
ENDIF

DEALLOCATE ( S_HF, S_HF_old, STAT=allocerror )
IF ( allocerror > 0 ) THEN
  WRITE(*,*) 'ERROR: Memory for "S_HF, S_HF_old" could not be deallocated'
  STOP
ENDIF

DEALLOCATE ( density_G, STAT=allocerror )
IF ( allocerror > 0 ) THEN
  WRITE(*,*) 'ERROR: Memory for "density_G" could not be deallocated'
  STOP
ENDIF

CALL gf_iw_DEALLOCATE( G_HKcf )

!-----------------------------------------------------------------------------------------------------------------------------------

IF ( rank_0 ) THEN
  WRITE(6,*) 'Waiting for MPI tasks to finish ...'
ENDIF

CALL MPI_Barrier(MPI_COMM_WORLD, i_error)
IF ( rank_0 ) THEN
  WRITE(6,*) 'Done.'
ENDIF

CALL MPI_FINALIZE(i_error)

END PROGRAM main
