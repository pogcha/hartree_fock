MODULE math
!
!  Purpose:
!    Collection of mathematical routines
!
!  Record of revisions:
!      Date       Programmer	Description of change
!      ====       ==========	=====================
!    10/02/14     S. Barthel	Original code
!    28/08/15     + added routines called by Greens-function & Coulomb modules
!    14/09/15     + added hermitian eigenproblems
!

IMPLICIT NONE

! Data dictionary: declare constants

! Data dictionary: declare variable types, definitions, units

CONTAINS

  !=============================================================================================================================


recursive subroutine quicksort(a)
! quicksort.f -*-f90-*-
! Author: t-nissie
! License: GPLv3
! Gist: https://gist.github.com/t-nissie/479f0f16966925fa29ea
  implicit none
  real :: a(:)
  real x, t
  integer :: first = 1, last
  integer i, j

  last = size(a, 1)
  x = a( (first+last) / 2 )
  i = first
  j = last

  do
     do while (a(i) < x)
        i=i+1
     end do
     do while (x < a(j))
        j=j-1
     end do
     if (i >= j) exit
     t = a(i);  a(i) = a(j);  a(j) = t
     i=i+1
     j=j-1
  end do

  if (first < i - 1) call quicksort(a(first : i - 1))
  if (j + 1 < last)  call quicksort(a(j + 1 : last))
end subroutine quicksort
!
!  FUNCTION  FindMinimum( x, Start, En )
!      IMPLICIT  NONE
!      REAL, DIMENSION(1:), INTENT(IN) :: x
!      INTEGER, INTENT(IN)                :: Start, En
!      REAL                               :: Minimum
!      INTEGER                            :: Location
!      INTEGER                            :: i
!      INTEGER                            :: FindMinimum
!
!      Minimum  = x(Start)		! assume the first is the min
!      Location = Start			! record its position
!      DO i = Start+1, En		! start with next elements
!         IF (x(i) < Minimum) THEN	!   if x(i) less than the min?
!            Minimum  = x(i)		!      Yes, a new minimum found
!            Location = i                !      record its position
!         END IF
!      END DO
!      FindMinimum = Location        	! return the position
!   END FUNCTION  FindMinimum
!
!
!   SUBROUTINE  Swap(a, b)
!      IMPLICIT  NONE
!      REAL, INTENT(INOUT) :: a, b
!      REAL                :: Temp
!
!      Temp = a
!      a    = b
!      b    = Temp
!   END SUBROUTINE  Swap
!
!
!   SUBROUTINE  Sort(x, Size)
!      IMPLICIT  NONE
!      REAL, DIMENSION(1:), INTENT(INOUT) :: x
!      INTEGER, INTENT(IN)                   :: Size
!      INTEGER                               :: i
!      INTEGER                               :: Location
!
!      DO i = 1, Size-1			! except for the last
!         Location = FindMinimum(x, i, Size)	! find min from this to last
!         CALL  Swap(x(i), x(Location))	! swap this and the minimum
!      END DO
!   END SUBROUTINE  Sort


  FUNCTION factorial( n )
    !
    !  Purpose:
    !    Calculate the factorial n! for a given (positive) integer n
    !
    !  Record of revisions:
    !      Date       Programmer	Description of change
    !      ====       ==========	=====================
    !    11/08/14     S. Barthel	Original code
    !
    IMPLICIT NONE

    ! Data dictionary: declare constants

    ! Data dictionary: declare variable types, definitions, units
    INTEGER, INTENT(IN) :: n
    INTEGER :: i
    REAL :: factorial

!     IF ( n < 0 ) THEN
!       STOP
!     ENDIF

    factorial = 1 ! 0!
    DO i=1,n
      factorial=factorial*i
    ENDDO

  END FUNCTION factorial

  !=============================================================================================================================

  FUNCTION wigner_3j( j1, j2, j3, m1, m2, m3)
    !
    !  Purpose: Calculate the Wigner-3j-Symbol (input is integer only)
    !
    !  Source: http://functions.wolfram.com/HypergeometricFunctions/ThreeJSymbol/02/,
    !          http://functions.wolfram.com/07.39.06.0004.01
    !          IMPORTANT : The summation restriction in this source is NOT correct though, see comment below and
    !                      http://en.wikipedia.org/wiki/Table_of_Clebsch%E2%80%93Gordan_coefficients
    !
    !  Record of revisions:
    !      Date       Programmer	Description of change
    !      ====       ==========	=====================
    !    29/05/15     S. Barthel	Original code
    !

    IMPLICIT NONE

    ! Data dictionary: declare constants

    ! Data dictionary: declare variable types, definitions, units

    REAL :: wigner_3j
    REAL :: sum_tmp, sgn

    INTEGER, INTENT(IN) :: j1, j2, j3, m1, m2, m3
    INTEGER :: k

    !---------------------------------------------------------------------------------------------------------------------------

    !Selection rules
    wigner_3j = 0.

    IF ( m1 < -ABS(j1) .OR. m1 > ABS(j1) ) THEN
      RETURN
    ENDIF
    IF ( m2 < -ABS(j2) .OR. m2 > ABS(j2) ) THEN
      RETURN
    ENDIF
    IF ( m3 < -ABS(j3) .OR. m3 > ABS(j3) ) THEN
      RETURN
    ENDIF

    IF ( m1+m2+m3 /= 0 ) THEN
      RETURN
    ENDIF

    !Triangular rule
    IF ( (2*j1<0) .OR. (2*j2<0) .OR. (2*j3<0) .OR. ((j1+j2+j3)<0) ) THEN
      RETURN
    ENDIF
    IF ( (j3<ABS(j1-j2)) .OR. (j3>(j1+j2)) ) THEN
      RETURN
    ENDIF

    IF ( MOD(j2-j1+m3,2) /= 0 ) THEN
      wigner_3j = -1.0
    ELSE
      wigner_3j = 1.0
    ENDIF

    wigner_3j = wigner_3j * SQRT( factorial(j1+j2-j3)*factorial(j1-j2+j3)*factorial(-j1+j2+j3) / factorial(j1+j2+j3+1) )
    wigner_3j = wigner_3j * SQRT( factorial(j1+m1)*factorial(j1-m1) &
                                 *factorial(j2+m2)*factorial(j2-m2) &
                                 *factorial(j3-m3)*factorial(j3+m3) )

    !-------------------------------------------------------------------------------------------------------------------------------
    !Summation over k extends over all integer k for which the argument of every factorial is nonnegative
    !Minimum is upper bound: j1+j2-j3 >= k, j1-m1 >= k, j2+m2 >= k
    !Maximum is lower bound: 0 <= k, -j3+j2-m1 <= k, -j3+j1+m2 <= k
    !Note: The source is missing one term: j1+j2-j3 >= k
    !-------------------------------------------------------------------------------------------------------------------------------
    sum_tmp = 0.
    DO k=MAX(0,-j3+j2-m1,-j3+j1+m2),MIN(j1+j2-j3,j1-m1,j2+m2)
      IF (MOD(k,2) /= 0 ) THEN
        sgn = -1.0
      ELSE
        sgn = 1.0
      ENDIF
      sum_tmp = sum_tmp + sgn / ( factorial(k) * factorial(j1+j2-j3-k) * factorial(j1-m1-k) &
                              * factorial(j2+m2-k) * factorial(j3-j2+m1+k) * factorial(j3-j1-m2+k) )
    ENDDO

    wigner_3j = wigner_3j * sum_tmp

  END FUNCTION wigner_3j

  !=============================================================================================================================

  FUNCTION gaunt( l1, l2, l3, m1, m2, m3 )
    !
    !  Purpose: Calculate the Gaunt-coefficient/integral over three spherical harmonics
    !           ∫ Y_{l1}^{m1}(θ,φ)*Y_{l2}^{m2}(θ,φ)*Y_{l3}^{m3}(θ,φ) dθdφ using the Wigner-3j-Symbols
    !
    !  Source: http://mathworld.wolfram.com/SphericalHarmonic.html, Equation (13), 31.05.2015
    !
    !  Record of revisions:
    !      Date       Programmer	Description of change
    !      ====       ==========	=====================
    !    29/05/15     S. Barthel	Original code
    !

    USE constants

    IMPLICIT NONE

    ! Data dictionary: declare constants

    ! Data dictionary: declare variable types, definitions, units
    INTEGER, INTENT(IN) :: l1, l2, l3, m1, m2, m3
    REAL :: gaunt

    gaunt = SQRT((2*l1+1)*(2*l2+1)*(2*l3+1)/(4*PI))*wigner_3j(l1,l2,l3,0,0,0)*wigner_3j(l1,l2,l3,m1,m2,m3)

  END FUNCTION gaunt

  !=============================================================================================================================

  FUNCTION cross_product(u,v)
    !
    !  Purpose:
    !    Calculate the cross-product w = u x v for two given vectors u, v
    !
    !  Record of revisions:
    !      Date       Programmer	Description of change
    !      ====       ==========	=====================
    !    10/02/14     S. Barthel	Original code
    !
    IMPLICIT NONE

    ! Data dictionary: declare constants

    ! Data dictionary: declare variable types, definitions, units

    REAL, DIMENSION(3), INTENT(IN) :: u, v	!input vectors
    REAL, DIMENSION(3) :: cross_product	!result

    cross_product(1) = u(2)*v(3)-u(3)*v(2)
    cross_product(2) = u(3)*v(1)-u(1)*v(3)
    cross_product(3) = u(1)*v(2)-u(2)*v(1)

  END FUNCTION cross_product

  !=============================================================================================================================

  SUBROUTINE complex_square_matrix_qr(dim_matrix, matrix)
    !
    !  Purpose:
    !    Calculate the QR factorization
    !    of a SQUARE COMPLEX matrix using BLAS/LAPACK
    !
    !  Record of revisions:
    !      Date       Programmer	Description of change
    !      ====       ==========	=====================
    !    16/02/15     S. Barthel	Original code
    !
    IMPLICIT NONE

    ! Data dictionary: declare constants

    ! Data dictionary: declare variable types, definitions, units
    INTEGER :: allocerror, ioerror						!error codes for allocation and file IO
    INTEGER, INTENT(IN) :: dim_matrix						!Dimension of the input matrix
    COMPLEX, DIMENSION(dim_matrix,dim_matrix), INTENT(INOUT) :: matrix		!Input matrix (will be modified on exit)
    COMPLEX, DIMENSION(dim_matrix) :: tau					!Scalar factors og the elementary reflectors
    INTEGER :: dim_work								!Dimension of the temporary work array
    COMPLEX, ALLOCATABLE, DIMENSION(:) :: work					!Temporary work array
    INTEGER :: info								!Error flag

    !Optimal work array size
    ALLOCATE ( work(dim_matrix), STAT=allocerror )
    IF ( allocerror > 0 ) THEN
      WRITE(*,*) 'ERROR: Memory for "work" could not be allocated'
      STOP
    ELSE
      tau = CMPLX(0.,0.)
      work = CMPLX(0.,0.)
      CALL ZGEQRF( dim_matrix, dim_matrix, matrix, dim_matrix, tau, work, -1, info )
      IF ( info /= 0 ) THEN
	WRITE(6,*) 'ERROR: ZGEQRF failed !', info, dim_work
	STOP
      ENDIF
      dim_work = work(1)
      !WRITE(6,*) dim_work
      DEALLOCATE ( work, STAT=allocerror )
      IF ( allocerror > 0 ) THEN
	WRITE(*,*) 'ERROR: Memory for "work" could not be deallocated'
	STOP
      ENDIF
    ENDIF

    !First call
    ALLOCATE ( work(dim_work), STAT=allocerror )
    IF ( allocerror > 0 ) THEN
      WRITE(*,*) 'ERROR: Memory for "work" could not be allocated'
      STOP
    ELSE
      tau = CMPLX(0.,0.)
      work = CMPLX(0.,0.)
      CALL ZGEQRF( dim_matrix, dim_matrix, matrix, dim_matrix, tau, work, dim_work, info )
      IF ( info /= 0 ) THEN
	WRITE(6,*) 'ERROR: ZGEQRF failed !', info, dim_work
	STOP
      ENDIF
      DEALLOCATE ( work, STAT=allocerror )
      IF ( allocerror > 0 ) THEN
	WRITE(*,*) 'ERROR: Memory for "work" could not be deallocated'
	STOP
      ENDIF
    ENDIF

    !--

    !Optimal work array size
    ALLOCATE ( work(dim_matrix), STAT=allocerror )
    IF ( allocerror > 0 ) THEN
      WRITE(*,*) 'ERROR: Memory for "work" could not be allocated'
      STOP
    ELSE
      work = CMPLX(0.,0.)
      CALL ZUNGQR( dim_matrix, dim_matrix, dim_matrix, matrix, dim_matrix, tau, work, -1, info )
      IF ( info /= 0 ) THEN
	WRITE(6,*) 'ERROR: ZUNGQR failed !', info, dim_work
	STOP
      ENDIF
      dim_work = work(1)
      !WRITE(6,*) dim_work
      DEALLOCATE ( work, STAT=allocerror )
      IF ( allocerror > 0 ) THEN
	WRITE(*,*) 'ERROR: Memory for "work" could not be deallocated'
	STOP
      ENDIF
    ENDIF

    !Second call
    ALLOCATE ( work(dim_work), STAT=allocerror )
    IF ( allocerror > 0 ) THEN
      WRITE(*,*) 'ERROR: Memory for "work" could not be allocated'
      STOP
    ELSE
      work = CMPLX(0.,0.)
      CALL ZUNGQR( dim_matrix, dim_matrix, dim_matrix, matrix, dim_matrix, tau, work, dim_work, info )
      IF ( info /= 0 ) THEN
	WRITE(6,*) 'ERROR: ZUNGQR failed !', info, dim_work
	STOP
      ENDIF
      DEALLOCATE ( work, STAT=allocerror )
      IF ( allocerror > 0 ) THEN
	WRITE(*,*) 'ERROR: Memory for "work" could not be deallocated'
	STOP
      ENDIF
    ENDIF

  END SUBROUTINE complex_square_matrix_qr

  !=============================================================================================================================

  SUBROUTINE real_matrix_inverse(dim_matrix, matrix)
    !
    !  Purpose:
    !    Calculate the inverse of a general real square matrix using BLAS/LAPACK
    !
    !  Record of revisions:
    !      Date       Programmer	Description of change
    !      ====       ==========	=====================
    !    23/06/15     S. Barthel	Original code
    !
    IMPLICIT NONE

    ! Data dictionary: declare constants

    ! Data dictionary: declare variable types, definitions, units
    INTEGER, INTENT(IN) :: dim_matrix
    REAL, DIMENSION(dim_matrix,dim_matrix), INTENT(INOUT) :: matrix

    INTEGER, DIMENSION(dim_matrix) :: idx_pivot
    INTEGER :: info

    REAL, DIMENSION(dim_matrix) :: work

    !compute LU factorization
    CALL DGETRF( dim_matrix, dim_matrix, matrix, dim_matrix, idx_pivot, info )
    IF ( info /= 0 ) THEN
      WRITE(6,*) 'ERROR: DGETRF failed !', info
      STOP
    ENDIF

    !compute the inverse using the LU factorization
    CALL DGETRI( dim_matrix, matrix, dim_matrix, idx_pivot, work, dim_matrix, info)
    IF ( info /= 0 ) THEN
      WRITE(6,*) 'ERROR: DGETRI failed !', info
      STOP
    ENDIF

  END SUBROUTINE real_matrix_inverse

  !=============================================================================================================================

  SUBROUTINE complex_matrix_inverse(dim_matrix, matrix)
    !
    !  Purpose:
    !    Calculate the inverse of a general complex square matrix using BLAS/LAPACK
    !
    !  Record of revisions:
    !      Date       Programmer	Description of change
    !      ====       ==========	=====================
    !    03/03/14     S. Barthel	Original code
    !
    IMPLICIT NONE

    ! Data dictionary: declare constants

    ! Data dictionary: declare variable types, definitions, units
    INTEGER, INTENT(IN) :: dim_matrix
    COMPLEX, DIMENSION(dim_matrix,dim_matrix), INTENT(INOUT) :: matrix

    INTEGER, DIMENSION(dim_matrix) :: idx_pivot
    INTEGER :: info

    COMPLEX, DIMENSION(dim_matrix) :: work

    !compute LU factorization
    CALL ZGETRF( dim_matrix, dim_matrix, matrix, dim_matrix, idx_pivot, info )
    IF ( info /= 0 ) THEN
      WRITE(6,*) 'ERROR: ZGETRF failed !', info
      STOP
    ENDIF

    !compute the inverse using the LU factorization
    CALL ZGETRI( dim_matrix, matrix, dim_matrix, idx_pivot, work, dim_matrix, info)
    IF ( info /= 0 ) THEN
      WRITE(6,*) 'ERROR: ZGETRI failed !', info
      STOP
    ENDIF

  END SUBROUTINE complex_matrix_inverse

  !=============================================================================================================================

  FUNCTION determinant(dim_matrix,matrix)
    !
    !  Purpose:
    !    To evaluate the determinant of a general complex square matrix
    !
    !  Record of revisions:
    !      Date       Programmer	Description of change
    !      ====       ==========	=====================
    !    03/03/15     S. Barthel	Original code
    !
    IMPLICIT NONE

    ! Data dictionary: declare constants

    ! Data dictionary: declare variable types, definitions, units

    INTEGER, INTENT(IN) :: dim_matrix
    COMPLEX, INTENT(IN), DIMENSION(dim_matrix,dim_matrix) :: matrix

    COMPLEX :: determinant

    INTEGER, DIMENSION(dim_matrix) :: idx_pivot
    INTEGER :: info

    INTEGER :: i
    REAL :: sgn

    !-------------------------------------------------------------------------------------------------------------------------------

    idx_pivot = 0
    !compute LU factorization
    CALL ZGETRF( dim_matrix, dim_matrix, matrix, dim_matrix, idx_pivot, info )
    IF ( info /= 0 ) THEN
      WRITE(6,*) 'ERROR: ZGETRF failed !', info
      STOP
    ENDIF
    determinant = CMPLX(1.,0.)
    sgn = 1.0
    DO i=1,dim_matrix
      determinant = determinant*matrix(i,i)
      IF ( idx_pivot(i) /= i) THEN
	sgn = -sgn
      ENDIF
      determinant=sgn*determinant
    END DO

  END FUNCTION determinant

  !=============================================================================================================================

  SUBROUTINE complex_matrix_inverse_diag(dim_matrix, matrix)
    !
    !  Purpose:
    !    Calculate the inverse of a general complex square matrix using BLAS/LAPACK
    !
    !  Record of revisions:
    !      Date       Programmer	Description of change
    !      ====       ==========	=====================
    !    03/03/14     S. Barthel	Original code
    !
    IMPLICIT NONE

    ! Data dictionary: declare constants

    ! Data dictionary: declare variable types, definitions, units
    INTEGER, INTENT(IN) :: dim_matrix
    COMPLEX, DIMENSION(dim_matrix,dim_matrix), INTENT(INOUT) :: matrix

    COMPLEX :: det
    COMPLEX, DIMENSION(dim_matrix,dim_matrix) :: matrix_tmp
    COMPLEX, DIMENSION(dim_matrix) :: inv_diag
    INTEGER :: i
    !-------------------------------------------------------------------------------------------------------------------------------

    !calculate determinant of full matrix
    det = determinant(dim_matrix,matrix)
    !WRITE(6,*) det

    !modify columns and calculate inverse matrix element using Cramer's rule
    DO i=1,dim_matrix
      matrix_tmp = matrix
      matrix_tmp(:,i) = CMPLX(0.,0.)
      matrix_tmp(i,i) = CMPLX(1.,0.)
      inv_diag(i) = determinant(dim_matrix,matrix_tmp) / det
    ENDDO

    matrix = CMPLX(0.,0.)
    DO i=1,dim_matrix
      matrix(i,i) = inv_diag(i)
    ENDDO

  END SUBROUTINE complex_matrix_inverse_diag

  !=============================================================================================================================

  SUBROUTINE trans_REAL_EIG_TO_BASIS_HE(dim_matrix,diag,ev,matrix)
    !
    !  Purpose:
    !    Transform a diagonal matrix (given as a 1 dimensional vector containing the real diagonal elements) to the original basis given by eigenvectors
    !
    !  Record of revisions:
    !      Date       Programmer	Description of change
    !      ====       ==========	=====================
    !    07/04/14     S. Barthel	Original code
    !
    INTEGER, INTENT(IN)                     :: dim_matrix

    REAL, INTENT(IN), DIMENSION(dim_matrix) :: diag

    COMPLEX, INTENT(IN), DIMENSION(dim_matrix,dim_matrix) :: ev


    COMPLEX, INTENT(OUT), DIMENSION(dim_matrix,dim_matrix) :: matrix



    INTEGER :: orbital_i, orbital_j,  orbital_k			!loop indices


    DO orbital_i=1,dim_matrix
      DO orbital_j=1,dim_matrix
        DO orbital_k=1,dim_matrix

            matrix(orbital_i, orbital_j) = matrix(orbital_i, orbital_j)  &
            + (ev(orbital_i,orbital_k))*diag(orbital_k)*CONJG(ev(orbital_j,orbital_k))

        ENDDO
      ENDDO
    ENDDO

  END SUBROUTINE

  SUBROUTINE complex_matrix_diagonalization(dim_matrix, matrix, ew, ev_L, ev_R, dim_work)
    !
    !  Purpose:
    !    Calculate the eigenvalues & eigenvectors of a general complex square matrix using BLAS/LAPACK
    !
    !  Record of revisions:
    !      Date       Programmer	Description of change
    !      ====       ==========	=====================
    !    07/04/14     S. Barthel	Original code
    !
    IMPLICIT NONE

    ! Data dictionary: declare constants

    ! Data dictionary: declare variable types, definitions, units
    INTEGER, INTENT(IN) :: dim_matrix
    COMPLEX, DIMENSION(dim_matrix,dim_matrix), INTENT(INOUT) :: matrix

    COMPLEX, DIMENSION(dim_matrix), INTENT(INOUT) :: ew
    COMPLEX, DIMENSION(dim_matrix,dim_matrix), INTENT(INOUT) :: ev_L
    COMPLEX, DIMENSION(dim_matrix,dim_matrix), INTENT(INOUT) :: ev_R

    INTEGER, INTENT(INOUT) :: dim_work
    COMPLEX, DIMENSION( MAX(1,dim_work) ) :: work
    REAL, DIMENSION(2*dim_matrix) :: rwork

    INTEGER :: info

    !solve eigenvalue problem
    CALL ZGEEV( 'N', 'V', dim_matrix, matrix, dim_matrix, ew, ev_L, dim_matrix, ev_R, dim_matrix, work, dim_work, rwork, info )
    IF ( info /= 0 ) THEN
      WRITE(6,*) 'ERROR: ZGEEV failed !', info
      STOP
    ENDIF

    !WRITE(6,*) info

    !calculate optimal size of the work array
    IF ( dim_work == -1 ) THEN
      dim_work = work(1)
    ENDIF

  END SUBROUTINE complex_matrix_diagonalization

  !=============================================================================================================================

  SUBROUTINE hermitian_matrix_diagonalization(dim_matrix, matrix, ew, dim_work)
    !
    !  Purpose:
    !    Calculate the eigenvalues & eigenvectors of a hermitian complex square matrix using BLAS/LAPACK
    !
    !  Record of revisions:
    !      Date       Programmer	Description of change
    !      ====       ==========	=====================
    !    07/04/14     S. Barthel	Original code
    !    12/09/15     -ZGEEV, +ZHEEV : ZGEEV may not ensure orthogonality (not tested), but ZHEEV should be faster anyways ...
    !
    IMPLICIT NONE

    ! Data dictionary: declare constants

    ! Data dictionary: declare variable types, definitions, units

    !ZHEEV
    INTEGER, INTENT(IN) :: dim_matrix
    COMPLEX, DIMENSION(dim_matrix,dim_matrix), INTENT(INOUT) :: matrix
    REAL, DIMENSION(dim_matrix), INTENT(INOUT) :: ew
    INTEGER, INTENT(INOUT) :: dim_work                !needs to be placer before work
    COMPLEX, DIMENSION( MAX(1,dim_work) ) :: work
    REAL, DIMENSION( MAX(1,3*dim_matrix-2) ) :: rwork
    INTEGER :: info

    CALL ZHEEV 	( 'V', 'U', dim_matrix, matrix, dim_matrix, ew, work, dim_work, rwork, info )
    IF ( info /= 0 ) THEN
      WRITE(6,*) 'ERROR: ZGEEV failed !', info
      STOP
    ENDIF

    !WRITE(6,*) info

    !calculate optimal size of the work array
    IF ( dim_work == -1 ) THEN
      dim_work = work(1)
    ENDIF

  END SUBROUTINE hermitian_matrix_diagonalization

  !=============================================================================================================================

  SUBROUTINE complex_matrix_eigenvalues(dim_matrix, matrix, ew, dim_work)
    !
    !  Purpose:
    !    Calculate the eigenvalues & eigenvectors of a general complex square matrix using BLAS/LAPACK
    !
    !  Record of revisions:
    !      Date       Programmer	Description of change
    !      ====       ==========	=====================
    !    07/04/14     S. Barthel	Original code
    !
    IMPLICIT NONE

    ! Data dictionary: declare constants

    ! Data dictionary: declare variable types, definitions, units
    INTEGER, INTENT(IN) :: dim_matrix
    COMPLEX, DIMENSION(dim_matrix,dim_matrix), INTENT(INOUT) :: matrix

    COMPLEX, DIMENSION(dim_matrix), INTENT(INOUT) :: ew
    COMPLEX, DIMENSION(dim_matrix,dim_matrix) :: ev_L
    COMPLEX, DIMENSION(dim_matrix,dim_matrix) :: ev_R

    INTEGER, INTENT(INOUT) :: dim_work
    COMPLEX, DIMENSION( MAX(1,dim_work) ) :: work
    REAL, DIMENSION(2*dim_matrix) :: rwork

    INTEGER :: info

    !solve eigenvalue problem but do not calculate eigenvectors
    CALL ZGEEV( 'N', 'N', dim_matrix, matrix, dim_matrix, ew, ev_L, dim_matrix, ev_R, dim_matrix, work, dim_work, rwork, info )
    IF ( info /= 0 ) THEN
      WRITE(6,*) 'ERROR: ZGEEV failed !', info
      STOP
    ENDIF

    !WRITE(6,*) info

    !calculate optimal size of the work array
    IF ( dim_work == -1 ) THEN
      dim_work = work(1)
    ENDIF

  END SUBROUTINE complex_matrix_eigenvalues

  !=============================================================================================================================

  SUBROUTINE hermitian_matrix_eigenvalues(dim_matrix, matrix, ew, dim_work)
    !
    !  Purpose:
    !    Calculate the eigenvalues & eigenvectors of a hermitian complex square matrix using BLAS/LAPACK
    !
    !  Record of revisions:
    !      Date       Programmer	Description of change
    !      ====       ==========	=====================
    !    07/04/14     S. Barthel	Original code
    !    12/09/15     -ZGEEV, +ZHEEV : ZGEEV may not ensure orthogonality (not tested), but ZHEEV should be faster anyways ...
    !
    IMPLICIT NONE

    ! Data dictionary: declare constants

    ! Data dictionary: declare variable types, definitions, units

    !ZHEEV
    INTEGER, INTENT(IN) :: dim_matrix
    COMPLEX, DIMENSION(dim_matrix,dim_matrix), INTENT(INOUT) :: matrix
    REAL, DIMENSION(dim_matrix), INTENT(INOUT) :: ew
    INTEGER, INTENT(INOUT) :: dim_work                !needs to be placer before work
    COMPLEX, DIMENSION( MAX(1,dim_work) ) :: work
    REAL, DIMENSION( MAX(1,3*dim_matrix-2) ) :: rwork
    INTEGER :: info

    CALL ZHEEV 	( 'N', 'U', dim_matrix, matrix, dim_matrix, ew, work, dim_work, rwork, info )
    IF ( info /= 0 ) THEN
      WRITE(6,*) 'ERROR: ZGEEV failed !', info
      STOP
    ENDIF

    !WRITE(6,*) info

    !calculate optimal size of the work array
    IF ( dim_work == -1 ) THEN
      dim_work = work(1)
    ENDIF

  END SUBROUTINE hermitian_matrix_eigenvalues

  !=============================================================================================================================

  SUBROUTINE complex_square_matrix_svd(dim_matrix, matrix, sw, matrix_v_hc, dim_work)
    !
    !  Purpose:
    !    Calculate the singular value decomposition & hermitian-conjugate of the right singular vectors
    !    of a SQUARE COMPLEX matrix using BLAS/LAPACK
    !
    !  Record of revisions:
    !      Date       Programmer	Description of change
    !      ====       ==========	=====================
    !    17/04/14     S. Barthel	Original code
    !
    IMPLICIT NONE

    ! Data dictionary: declare constants

    ! Data dictionary: declare variable types, definitions, units
    INTEGER, INTENT(IN) :: dim_matrix						!Dimension of the input matrix
    COMPLEX, DIMENSION(dim_matrix,dim_matrix), INTENT(INOUT) :: matrix	!Input matrix

    REAL, DIMENSION(dim_matrix), INTENT(INOUT) :: sw				!Singular values

    COMPLEX, DIMENSION(dim_matrix,dim_matrix) :: matrix_u 			!Left singular-vectors, not referenced
    COMPLEX, DIMENSION(dim_matrix,dim_matrix), INTENT(INOUT) :: matrix_v_hc	!Hermitian-conjugate of right singular-vectors

    INTEGER, INTENT(INOUT) :: dim_work						!Dimension of the temporary work array
    COMPLEX, DIMENSION( MAX(1,dim_work) ) :: work				!Temporary work array
    REAL, DIMENSION(5*dim_matrix) :: rwork					!Another temporary work array

    INTEGER :: info								!Error flag

    !Calculate singular value decomposition
    CALL ZGESVD( 'N', 'A', dim_matrix, dim_matrix, matrix, dim_matrix, sw, matrix_u, dim_matrix, matrix_v_hc, &
      dim_matrix, work, dim_work, rwork, info )
    IF ( info /= 0 ) THEN
      WRITE(6,*) 'ERROR: ZGESVD failed !', info, dim_work
      STOP
    ENDIF

    !WRITE(6,*) info

    !Calculate optimal size of the work array
    IF ( dim_work == -1 ) THEN
      dim_work = work(1)
    ENDIF

  END SUBROUTINE complex_square_matrix_svd

  !=============================================================================================================================

  SUBROUTINE real_least_square_fit( dim_m, y, x, dim_p, dim_work )
    !
    !  Purpose: Performs a polynomial least square regression of order dim_p
    !
    !  Record of revisions:
    !      Date       Programmer	Description of change
    !      ====       ==========	=====================
    !    14/05/14     S. Barthel	Original code
    !
    IMPLICIT NONE

    ! Data dictionary: declare constants

    ! Data dictionary: declare variable types, definitions, units
    INTEGER, INTENT(IN) :: dim_m					!Dimension of the input data vectors
    INTEGER, INTENT(IN) :: dim_p					!Maximum order of the polynomial to fit
    REAL, DIMENSION(dim_m,dim_p) :: matrix_A
    REAL, DIMENSION(dim_m), INTENT(INOUT) :: y				!Data to fit y(x), will be overwritten with solution
    REAL, DIMENSION(dim_m), INTENT(IN) :: x				!Variable of data to fit x
    INTEGER, INTENT(INOUT) :: dim_work					!Dimension of the temporary work array
    REAL, DIMENSION( MAX(1,dim_work) ) :: work				!Temporary work array
    INTEGER :: info							!Error flag
    INTEGER :: i,p							!Loop indices

    REAL, DIMENSION(dim_p) :: sv					!Singular values
    REAL :: RCOND = -1.D0 						!This equates to machine precision
    INTEGER :: rank_A

    !Setup real linear least squares problem for a polynomial regression
    DO p=1,dim_p
      DO i=1,dim_m
	matrix_A(i,p) = x(i)**(p-1)
      ENDDO
    ENDDO

    !Compute the minimum norm solution to a real linear least squares problem using the singular value decomposition (SVD)
    CALL DGELSS( dim_m, dim_p, 1, matrix_A, dim_m, y, dim_m, sv, RCOND, rank_A, work, dim_work, info)
    !CAll DGELS( 'N', dim_m, dim_p, 1, matrix_A, dim_m, y, dim_m, work, dim_work, info )
    IF ( info /= 0 ) THEN
      WRITE(6,*) 'ERROR: DGELSS failed !', info
      STOP
    ENDIF

    !Calculate optimal size of the work array
    IF ( dim_work == -1 ) THEN
      dim_work = work(1)
    ENDIF

  END SUBROUTINE real_least_square_fit

  !=============================================================================================================================

  SUBROUTINE complex_least_square_fit( dim_m, y, x, dim_p, dim_work )
    !
    !  Purpose: Performs a polynomial least square regression of order (dim_p-1)
    !
    !  Record of revisions:
    !      Date       Programmer	Description of change
    !      ====       ==========	=====================
    !    14/05/14     S. Barthel	Original code
    !
    IMPLICIT NONE

    ! Data dictionary: declare constants

    ! Data dictionary: declare variable types, definitions, units
    INTEGER, INTENT(IN) :: dim_m					!Dimension of the input data vectors
    INTEGER, INTENT(IN) :: dim_p					!Maximum order of the polynomial to fit
    COMPLEX, DIMENSION(dim_m,dim_p) :: matrix_A
    COMPLEX, DIMENSION(dim_m), INTENT(INOUT) :: y			!Data to fit y(x), will be overwritten with solution
    COMPLEX, DIMENSION(dim_m), INTENT(IN) :: x				!Variable of data to fit x
    INTEGER, INTENT(INOUT) :: dim_work					!Dimension of the temporary work array
    COMPLEX, DIMENSION( MAX(1,dim_work) ) :: work			!Temporary work array
    INTEGER :: info							!Error flag
    INTEGER :: i,p							!Loop indices

    REAL, DIMENSION(dim_p) :: sv					!Singular values
    REAL :: RCOND = -1.D0 						!This equates to machine precision
    INTEGER :: rank_A
    REAL, DIMENSION(5*MIN(dim_M,dim_p)) ::rwork

    !Setup real linear least squares problem for a polynomial regression, TODO: this needs to be changed to dim_p instead of (dim_p-1)
    DO p=1,dim_p
      DO i=1,dim_m
	matrix_A(i,p) = x(i)**(p-1)
      ENDDO
    ENDDO

    !Compute the minimum norm solution to a real linear least squares problem using the singular value decomposition (SVD)
    CALL ZGELSS( dim_m, dim_p, 1, matrix_A, dim_m, y, dim_m, sv, RCOND, rank_A, work, dim_work, rwork, info)

    !CALL ZGELSS( M, N, NRHS, A, LDA, B, LDB, S, RCOND, RANK, WORK, LWORK, RWORK, INFO )
    !CAll DGELS( 'N', dim_m, dim_p, 1, matrix_A, dim_m, y, dim_m, work, dim_work, info )
    IF ( info /= 0 ) THEN
      WRITE(6,*) 'ERROR: ZGELSS failed !', info
      STOP
    ENDIF

    !Calculate optimal size of the work array
    IF ( dim_work == -1 ) THEN
      dim_work = work(1)
    ENDIF

  END SUBROUTINE complex_least_square_fit

  !=============================================================================================================================

  SUBROUTINE matsubara_sum(dim_iw, iw, Giw, n_reg, order_p, G, R2)
    !
    !  Purpose:
    !    Calculate the fermionic Matsubara sum using a polynomial regression of a cumulative sum
    !    for a given Matsubara Green's function Giw (no prefactor beta)
    !
    !  Record of revisions:
    !      Date       Programmer	Description of change
    !      ====       ==========	=====================
    !    26/03/14     S. Barthel	Original code
    !
    IMPLICIT NONE

    ! Data dictionary: declare constants

    ! Data dictionary: declare variable types, definitions, units
    INTEGER, INTENT(IN) :: dim_iw				!Number of Matsubara frequencies
    REAL, DIMENSION(dim_iw), INTENT(IN) :: iw			!Matsubara frequencies (imaginary part)
    COMPLEX, DIMENSION(dim_iw), INTENT(IN) :: Giw		!Matsubara Green's function
    INTEGER, INTENT(IN) :: n_reg				!Number of points used in the linear regression
    INTEGER, INTENT(IN) :: order_p				!Order of the polynomial fit
    COMPLEX, INTENT(OUT) :: G					!Matsubara sum without 1/beta prefactor

    REAL :: SS_res						!Residual sum of squares
    REAL :: SS_tot						!Total sum of squares
    REAL, INTENT(OUT) :: R2					!Coefficient of determination

    REAL, DIMENSION(n_reg) :: iw_rec_pos			!Reciprocal positive Matsubara frequencies
    REAL, DIMENSION(n_reg) :: cumsum_Giw_real			!Cumulative symmetric Matsubara sum
    REAL, DIMENSION(n_reg) :: cumsum_Giw_imag			!Cumulative symmetric Matsubara sum
    INTEGER :: i
    INTEGER :: dim_work_opt					!Optimal work array length

    !Calculate symmetric cumulative Matsubara sum starting with large frequency contributions up to frequency cutoff
    DO i=1,n_reg
      iw_rec_pos(i) = 1.D0 / iw(dim_iw-i+1)
      cumsum_Giw_real(i) = SUM( REAL(Giw( i:dim_iw-i+1 )) )
      cumsum_Giw_imag(i) = SUM( IMAG(Giw( i:dim_iw-i+1 )) )
    ENDDO

    !calculate total sum of squares
    !SS_tot = SUM((cumsum_Giw-(SUM(cumsum_Giw)/n_reg))**2)

    !Optimal work array length
    dim_work_opt = -1
    CALL real_least_square_fit( n_reg, cumsum_Giw_real, iw_rec_pos, order_p, dim_work_opt )
    !Perform polynomial regression
    CALL real_least_square_fit( n_reg, cumsum_Giw_real, iw_rec_pos, order_p, dim_work_opt )

    !Optimal work array length
    dim_work_opt = -1
    CALL real_least_square_fit( n_reg, cumsum_Giw_imag, iw_rec_pos, order_p, dim_work_opt )
    !Perform polynomial regression
    CALL real_least_square_fit( n_reg, cumsum_Giw_imag, iw_rec_pos, order_p, dim_work_opt )


    !First regression coefficent is the result
    G = CMPLX( cumsum_Giw_real(1), cumsum_Giw_imag(1) )
    R2 = -1.

    !the residual sum of squares for the solution in each column is given by the sum of squares of elements N+1 to M
    !SS_res = SUM( cumsum_Giw(order_p+1:n_reg)**2 )

    !coefficient of determination
    !R2 = 1.D0-(SS_res/SS_tot)

    !adjusted coefficient of determination
    !R2 = (SS_res/SS_tot) * (n_reg-1) / (n_reg-(order_p-1)-1)

  END SUBROUTINE matsubara_sum

  !=============================================================================================================================

  SUBROUTINE complex_matsubara_sum(dim_iw, iw, Giw, n_reg, order_p, G, R2)
    !
    !  Purpose:
    !    Calculate the fermionic Matsubara sum using a polynomial regression of a cumulative sum
    !    for a given Matsubara Green's function Giw (no prefactor beta)
    !
    !  Record of revisions:
    !      Date       Programmer	Description of change
    !      ====       ==========	=====================
    !    26/03/14     S. Barthel	Original code
    !
    IMPLICIT NONE

    ! Data dictionary: declare constants

    ! Data dictionary: declare variable types, definitions, units

    INTEGER :: ioerror

    INTEGER, INTENT(IN) :: dim_iw				!Number of Matsubara frequencies
    COMPLEX, DIMENSION(dim_iw), INTENT(IN) :: iw		!Matsubara frequencies
    COMPLEX, DIMENSION(dim_iw), INTENT(IN) :: Giw		!Matsubara Green's function
    INTEGER, INTENT(IN) :: n_reg				!Number of points used in the linear regression
    INTEGER, INTENT(IN) :: order_p				!Order of the polynomial fit
    COMPLEX, INTENT(OUT) :: G					!Matsubara sum without 1/beta prefactor

    REAL :: SS_res						!Residual sum of squares
    REAL :: SS_tot						!Total sum of squares
    REAL, INTENT(OUT) :: R2					!Coefficient of determination

    COMPLEX, DIMENSION(n_reg) :: iw_rec_pos			!Reciprocal positive Matsubara frequencies
    COMPLEX, DIMENSION(n_reg) :: cumsum_Giw			!Cumulative symmetric Matsubara sum
    INTEGER :: i
    INTEGER :: dim_work_opt					!Optimal work array length

    !Calculate symmetric cumulative Matsubara sum starting with large frequency contributions up to frequency cutoff
    !OPEN (UNIT=26,FILE='./CUMSUM.TXT',STATUS='REPLACE', ACTION='WRITE', IOSTAT=ioerror)
    DO i=1,n_reg
      iw_rec_pos(i) = 1. / iw(dim_iw-i+1)
      cumsum_Giw(i) = SUM( Giw( i:dim_iw-i+1 ) )
      !WRITE(26,'(3X,I6,1X,F19.16,1X,F19.16,1X,F19.16)') i, iw_rec_pos(i), cumsum_Giw_real(i), cumsum_Giw_imag(i)
    ENDDO
    !CLOSE(26)

    !calculate total sum of squares
    !SS_tot = SUM((cumsum_Giw-(SUM(cumsum_Giw)/n_reg))**2)

    !Optimal work array length
    dim_work_opt = -1
    CALL complex_least_square_fit( n_reg, cumsum_Giw, iw_rec_pos, order_p, dim_work_opt )
    !Perform polynomial regression
    CALL complex_least_square_fit( n_reg, cumsum_Giw, iw_rec_pos, order_p, dim_work_opt )

    !First regression coefficent is the result
    G = cumsum_Giw(1)
    R2 = -1.

    !the residual sum of squares for the solution in each column is given by the sum of squares of elements N+1 to M
    !SS_res = SUM( cumsum_Giw(order_p+1:n_reg)**2 )

    !coefficient of determination
    !R2 = 1.D0-(SS_res/SS_tot)

    !adjusted coefficient of determination
    !R2 = (SS_res/SS_tot) * (n_reg-1) / (n_reg-(order_p-1)-1)

  END SUBROUTINE complex_matsubara_sum

  !=============================================================================================================================

  SUBROUTINE overlap_matrix(dim_M, M, O, dim_sum)
    !
    !  Purpose: Calculate an overlap matrix O of complex square input matrix M with respect to a choosen dimension,
    !		i.e. O = M^{+} * M or O = M * M^{+}
    !
    !  Record of revisions:
    !      Date       Programmer	Description of change
    !      ====       ==========	=====================
    !    22/01/15     S. Barthel	Original code
    !
    IMPLICIT NONE

    ! Data dictionary: declare constants

    ! Data dictionary: declare variable types, definitions, units
    INTEGER, INTENT(IN) :: dim_M				!Dimension of the square input matrix M
    COMPLEX, DIMENSION(dim_M,dim_M), INTENT(IN) :: M		!Square input matrix M of dimension dim_M
    COMPLEX, DIMENSION(dim_M,dim_M), INTENT(OUT) :: O		!Square output overlap matrix O of dimension dim_M
    INTEGER :: i,j,k						!Loop indices
    INTEGER :: dim_sum						!Summation dimension of M

    !===========================================================================================================================
    !
    !===========================================================================================================================

    !Summation over first dimension of M, i.e. O = M^{+} * M
    IF ( dim_sum .EQ. 1 ) THEN

      DO j=1,dim_M
	DO i=1,dim_M
	  O(i,j) = CMPLX(0.,0.)				!initialize with zeros
	  DO k=1,dim_M
	    O(i,j) = O(i,j) + CONJG( M(k,i) ) * M(k,j)	!multiply & sum over bands -> orbital overlap matrix
	  ENDDO
	ENDDO
      ENDDO

    !Summation over second dimension of M, i.e. O = M * M^{+}
    ELSEIF ( dim_sum .EQ. 2 ) THEN

      DO j=1,dim_M
	DO i=1,dim_M
	  O(i,j) = CMPLX(0.,0.)				!initialize with zeros
	  DO k=1,dim_M
	    O(i,j) = O(i,j) + M(i,k) * CONJG( M(j,k) ) !multiply & sum over orbitals -> band overlap matrix
	  ENDDO
	ENDDO
      ENDDO

    ELSE
      WRITE(6,*) 'ERROR: Cannot calculate overlap matrix for this dimension.'
      STOP
    ENDIF

    !original implementation for comparison ...

!     DO band=1,n_bands_cr
!       sum_tmp = sum_tmp + CONJG(LKcr_orth(kpoint,band,orbital_prime)) * LKcr_orth(kpoint,band,orbital)
!     ENDDO
!     LKcr_orth_overlap(kpoint,orbital,orbital_prime) = sum_tmp !falscher index ... orbital_prime, orbital is correct
!
!     DO orbital=1,n_bands_cr
!       sum_tmp = sum_tmp + LKcr_orth(kpoint,band,orbital) * CONJG(LKcr_orth(kpoint,band_prime,orbital))
!     ENDDO
!     KLcr_orth_overlap(kpoint,band,band_prime) = sum_tmp	!ok

  END SUBROUTINE overlap_matrix

  !=============================================================================================================================

  SUBROUTINE overlap_matrix_orbitals(dim_M, dim_N, A, C)
    !
    !  Purpose: Calculate an overlap matrix C of complex rectangular input matrix A with respect to a choosen dimension,
    !		i.e. C = A^{+} * A or C = A * A^{+}
    !
    !  Record of revisions:
    !      Date       Programmer	Description of change
    !      ====       ==========	=====================
    !    22/01/15     S. Barthel	Original code
    !

    USE omp_lib

    IMPLICIT NONE

    ! Data dictionary: declare constants

    ! Data dictionary: declare variable types, definitions, units
    INTEGER, INTENT(IN) :: dim_M				!First dimension of the rectangular input matrix A
    INTEGER, INTENT(IN) :: dim_N				!Second dimension of the rectangular input matrix M
    COMPLEX, DIMENSION(dim_M,dim_N), INTENT(IN) :: A		!Rectangular input matrix A of dimension dim_M x dim_N

    COMPLEX, DIMENSION(dim_N,dim_N), INTENT(OUT) :: C		!Square output overlap matrix O of dimension dim_N x dim_N
    INTEGER :: i,j,k						!Loop indices

    !===========================================================================================================================
    !
    !===========================================================================================================================

    !$OMP PARALLEL DO COLLAPSE(2)
    DO j=1,dim_N
      DO i=1,dim_N
	C(i,j) = CMPLX(0.,0.)				!initialize with zeros
	DO k=1,dim_M
	  C(i,j) = C(i,j) + CONJG( A(k,i) ) * A(k,j)	!multiply & sum over bands -> orbital overlap matrix
	ENDDO
      ENDDO
    ENDDO
    !$OMP END PARALLEL DO

  END SUBROUTINE overlap_matrix_orbitals

  !=============================================================================================================================

END MODULE math
