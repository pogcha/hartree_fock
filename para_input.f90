MODULE para_input
  !
  !  Purpose:
  !    To read parameters formerly declared within 'shared_data.f90' from external file 'HF_PARA_INPUT'
  !
  !  Record of revisions:
  !      Date       Programmer	Description of change
  !      ====       ==========	=====================
  !    03/04/17     R. Pordzik	Original code
  !

   USE parallel
   USE import
   USE greens_functions

   CONTAINS

     SUBROUTINE read_parameters

        IMPLICIT NONE
        INTEGER :: ierr
        INTEGER :: ii, jj, kk, nn, mm, pp
        INTEGER, PARAMETER :: iu = 20
        CHARACTER (LEN=14), DIMENSION(19) :: search_str_num = (/"n_ions        ","beta          ","n_iw          ","mix_new_sigma ","n_iter        ","U_3d          ","J_3d          ","U             ","J             ","tol           ","idelta        ","mu_dft        ","mu_dc         ","dos_N         ","dos_min       ","dos_max       ","L_ang         ","T0            ","fit_start     "/)
        CHARACTER (LEN=16), DIMENSION(14) :: search_str_logical = (/"is_metallic     ","use_para        ","use_t2g_eg_sym  ","use_afm_sym     ","use_sym_ions    ","calc_G0_dos_only","use_sc_G        ","read_sigma      ","export_para     ","set_mu_dft      ","set_mu_dc       ","read_coulomb    ","dc_FLL          ","dc_AMF          "/)
        CHARACTER (LEN=6), DIMENSION(2) :: search_str_vec = (/"M     ","mu_int"/)
        LOGICAL, DIMENSION(34) :: search_log = .FALSE.
        REAL, DIMENSION(19) :: num_values
        LOGICAL, DIMENSION(14) :: logical_values
        REAL, DIMENSION(:,:), ALLOCATABLE :: vec_values
        INTEGER, DIMENSION(2) :: length_vec
        CHARACTER (LEN=1000) :: text
        CHARACTER (LEN=20) :: word
        CHARACTER (LEN=20) :: word_log
        INTEGER, DIMENSION(10) :: line
        CHARACTER (LEN=8) :: upper_case = "AEFLRSTU"
        CHARACTER (LEN=8) :: lower_case = "aeflrstu"

        OPEN (UNIT=iu,FILE='HF_PARA_INPUT', STATUS='OLD', ACTION='READ', IOSTAT=ierr)

        IF ( ierr /= 0 ) THEN
           IF (rank_0) THEN
              WRITE(6,*) 'Cannot find HF_PARA_INPUT!'
              WRITE(6,*) 'All parameters are set to default.'
           ENDIF
           ALLOCATE(M(n_ions))
           M=0
        ELSE
           DO ii = 1, SIZE(search_str_num)
              jj = 0
              kk=0
              DO WHILE (kk == 0)
                 READ (iu,"(a)",IOSTAT=ierr) text                           ! read line into CHARACTER variable
                 IF (ierr /= 0) EXIT
                 IF (text == "\n") CONTINUE
                 line(1)=1
                 line(2)=1000
                 nn=1
                 DO jj=1,LEN(text)
                    IF (text(jj:jj) == "#") THEN
                       text(jj:1000) = ""
                       text(jj:jj) = " "
                       EXIT
                    ELSEIF (text(jj:jj) == "=") THEN
                       text(jj:jj) = " "
                    ELSEIF (text(jj:jj) == ";") THEN
                       text(jj:jj)=" "
                       nn=nn+1
                       line(nn)=jj+1
                       line(nn+1)=1000
                    END IF
                 END DO
                 text(1000:1000)="*"
                 DO jj=2, nn+1
                    READ (text(line(jj-1):line(jj)),*) word                         ! read first word of line
                    IF (TRIM(word) == TRIM(search_str_num(ii))) THEN               ! found search string at beginning of line
                       READ (text(line(jj-1):line(jj)),*) word,num_values(ii)
                       search_log(ii)= .TRUE.
                       kk = 1
                    END IF
                 END DO
              END DO
              REWIND(iu)
           END DO

           IF (search_log(1) .EQV. .TRUE.) n_ions = int(num_values(1))
           IF (search_log(2) .EQV. .TRUE.) beta = num_values(2)
           IF (search_log(3) .EQV. .TRUE.) n_iw = int(num_values(3))
           IF (search_log(4) .EQV. .TRUE.) mix_new_sigma = num_values(4)
           IF (search_log(5) .EQV. .TRUE.) n_iter = int(num_values(5))
           IF (search_log(6) .EQV. .TRUE.) U_3d = num_values(6)
           IF (search_log(7) .EQV. .TRUE.) J_3d = num_values(7)
           IF (search_log(8) .EQV. .TRUE.) U = num_values(8)
           IF (search_log(9) .EQV. .TRUE.) J = num_values(9)
           IF (search_log(10) .EQV. .TRUE.) tol = num_values(10)
           IF (search_log(11) .EQV. .TRUE.) idelta = num_values(11)
           IF (search_log(12) .EQV. .TRUE.) mu_dftnew = num_values(12)
           IF (search_log(13) .EQV. .TRUE.) mu_dc = num_values(13)
           IF (search_log(14) .EQV. .TRUE.) dos_N = int(num_values(14))
           IF (search_log(15) .EQV. .TRUE.) dos_min = num_values(15)
           IF (search_log(16) .EQV. .TRUE.) dos_max = num_values(16)
           IF (search_log(17) .EQV. .TRUE.) L_ang = num_values(17)
           IF (search_log(18) .EQV. .TRUE.) T0 = num_values(18)
           IF (search_log(19) .EQV. .TRUE.) fit_start = num_values(19)
            L_max = 2*L_ang + 1

           ALLOCATE(M(n_ions))

           IF (n_ions > 3) THEN
              ALLOCATE(vec_values(2,n_ions))
           ELSE
              ALLOCATE(vec_values(2,3))
           END IF

           length_vec(1)= n_ions
           length_vec(2)= 3

           DO ii = 1, SIZE(search_str_logical)
              jj = 0
              kk=0
              DO WHILE (kk == 0)
                 READ (iu,"(a)",IOSTAT=ierr) text                           ! read line into CHARACTER variable
                 IF (ierr /= 0) EXIT
                 IF (text == "\n") CONTINUE
                 line(1)=1
                 line(2)=1000
                 nn=1
                 DO jj=1,LEN(text)
                    IF (text(jj:jj) == "#") THEN
                       text(jj:1000) = ""
                       text(jj:jj) = " "
                       EXIT
                    ELSEIF (text(jj:jj) == "=") THEN
                       text(jj:jj) = " "
                    ELSEIF (text(jj:jj) == ".") THEN
                       text(jj:jj) = " "
                    ELSEIF (text(jj:jj) == ";") THEN
                       text(jj:jj)=" "
                       nn=nn+1
                       line(nn)=jj+1
                       line(nn+1)=1000
                    END IF
                 END DO
                 text(1000:1000)="*"
                 DO jj=2, nn+1
                    READ (text(line(jj-1):line(jj)),*) word                          ! read first word of line
                    IF (TRIM(word) == TRIM(search_str_logical(ii))) THEN             ! found search string at beginning of line
                       READ (text(line(jj-1):line(jj)),*) word, word_log
                       search_log(ii+SIZE(search_str_num))= .TRUE.
                       DO mm = 1, LEN( word_log )
                          pp = INDEX( upper_case, word_log( mm:mm ) )
                          IF ( pp /= 0 ) word_log( mm:mm ) = lower_case( pp:pp )
                       END DO
                       IF (TRIM(word_log) == "true") THEN
                          logical_values(ii) = .TRUE.
                       ELSEIF (TRIM(word_log) == "false") THEN
                          logical_values(ii) = .FALSE.
                       kk = 1
                       ENDIF
                    END IF
                 END DO
              END DO
              REWIND(iu)
           END DO

           IF (search_log(20) .EQV. .TRUE.) is_metallic = logical_values(1)
           IF (search_log(21) .EQV. .TRUE.) use_para = logical_values(2)
           IF (search_log(22) .EQV. .TRUE.) use_t2g_eg_sym = logical_values(3)
           IF (search_log(23) .EQV. .TRUE.) use_afm_sym = logical_values(4)
           IF (search_log(24) .EQV. .TRUE.) use_sym_ions = logical_values(5)
           IF (search_log(25) .EQV. .TRUE.) calc_G0_dos_only = logical_values(6)
           IF (search_log(26) .EQV. .TRUE.) use_sc_G = logical_values(7)
           IF (search_log(27) .EQV. .TRUE.) read_sigma = logical_values(8)
           IF (search_log(28) .EQV. .TRUE.) export_para = logical_values(9)
           IF (search_log(29) .EQV. .TRUE.) set_mu_dft = logical_values(10)
           IF (search_log(30) .EQV. .TRUE.) set_mu_dc = logical_values(11)
           IF (search_log(31) .EQV. .TRUE.) read_coulomb = logical_values(12)
           IF (search_log(32) .EQV. .TRUE.) dc_FLL = logical_values(13)
           IF (search_log(33) .EQV. .TRUE.) dc_AMF = logical_values(14)

           IF ((dc_FLL == .FALSE.) .AND. (dc_AMF == .FALSE.) .AND. (set_mu_dc == .FALSE.)) THEN
                IF ( rank_0 ) THEN
                    WRITE(6,*) 'all double counting schemes set to FALSE... Exiting'

                ENDIF
                CALL EXIT()
           ENDIF
           IF ((dc_FLL == .TRUE.) .AND. (dc_AMF == .TRUE.) .AND. (set_mu_dc == .FALSE.)) THEN
                IF ( rank_0 ) THEN
                    WRITE(6,*) 'dc_AMF and dc_AMF can not be both set to TRUE... Exiting'
                ENDIF
                CALL EXIT()
           ENDIF
           DO ii = 1, SIZE(search_str_vec)
              jj = 0
              kk=0
              DO WHILE (kk == 0)
                 READ (iu,"(a)",IOSTAT=ierr) text                           ! read line into CHARACTER variable
                 IF (ierr /= 0) EXIT
                 IF (text == "\n") CONTINUE
                 line(1)=1
                 line(2)=1000
                 nn=1
                 DO jj=1,LEN(text)
                    IF (text(jj:jj) == "#") THEN
                       text(jj:1000) = ""
                       text(jj:jj) = " "
                       EXIT
                    ELSEIF (text(jj:jj) == "=") THEN
                            text(jj:jj) = " "
                    ELSEIF (text(jj:jj) == ",") THEN
                            text(jj:jj) = " "
                    ELSEIF (text(jj:jj) == "(") THEN
                            text(jj:jj) = " "
                    ELSEIF (text(jj:jj) == ")") THEN
                            text(jj:jj) = " "
                    ELSEIF (text(jj:jj) == "[") THEN
                            text(jj:jj) = " "
                    ELSEIF (text(jj:jj) == "]") THEN
                            text(jj:jj) = " "
                    ELSEIF (text(jj:jj) == ";") THEN
                       text(jj:jj)=" "
                       nn=nn+1
                       line(nn)=jj+1
                       line(nn+1)=1000
                    END IF
                 END DO
                 text(1000:1000)="*"
                 DO jj=2, nn+1
                    READ (text(line(jj-1):line(jj)),*) word                                       ! read first word of line
                    IF (TRIM(word) == TRIM(search_str_vec(ii))) THEN                              ! found search string at beginning of line
                       READ (text(line(jj-1):line(jj)),*) word, vec_values(ii,1:length_vec(ii))   !, vec_values(ii,2),  vec_values(ii,3)
                       search_log(ii+SIZE(search_str_num)+SIZE(search_str_logical))= .TRUE.
                       kk = 1
                    END IF
                 END DO
              END DO
              REWIND(iu)
           END DO

           IF (search_log(34) .EQV. .TRUE.) M = vec_values(1,1:n_ions)
           IF (search_log(34) .EQV. .FALSE.) M = 0
           IF (search_log(35) .EQV. .TRUE.) mu_int = vec_values(2,1:3)

           CLOSE(UNIT=iu)

           IF (rank_0) THEN
              WRITE(6,*) '-'
              WRITE(6,"(3X,'HF_PARA_INPUT found. The following parameters have been customized:')")
              DO ii=1, SIZE(search_log)
                 IF (ii<= SIZE(search_str_num)) THEN
                    IF (search_log(ii) .EQV. .TRUE.)  WRITE(6,*) TRIM(search_str_num(ii))
                 ELSEIF (ii<=SIZE(search_str_num)+SIZE(search_str_logical)) THEN
                    IF (search_log(ii) .EQV. .TRUE.) WRITE(6,*) TRIM(search_str_logical(ii-SIZE(search_str_num)))
                 ELSE
                    IF (search_log(ii) .EQV. .TRUE.) WRITE(6,*) TRIM(search_str_vec(ii-SIZE(search_str_num)-SIZE(search_str_logical)))
                 END IF
              END DO
              WRITE(6,*) '-'
           ENDIF

        END IF

        ALLOCATE(N_tot(n_ions,2))
        ALLOCATE(N_tot_0(n_ions,2))
        ALLOCATE(E_dc_fll(n_ions))

     END SUBROUTINE read_parameters

END MODULE para_input
