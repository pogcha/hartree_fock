ReadMe for the modified Hartree-Fock-Solver

- Parameters of calculation now accessible by new input file: HF_PARA_INPUT
- If distinct parameters are not specified within input file they are set to default values
- If HF_PARA_INPUT does not exist all parameters are automatically set to default
- Different parameters have to be seperated either by ";" or written to a new line
- Comments can be added after "#"
- Vector entries can be seperated be free space or by ","
  - They MUST NOT be shorter than required by other parameters
  - If they are longer they are simply cut off
- Logicals are not sensitive to upper or lower case letters
- Logicals can be specified either bey "true" or "false" or simply by "t" or "f"


Parameters:

beta			             !Inverse temperature

n_iw			             !Number of Matsubara frequencies
  
n_iter                               !Maximum number of iterations

mix_new_sigma			     !mixing factor for new self-energy

idelta                               !Lorentzian broadening of the DOS lines

mu_dft			             !Fixing the chemical potential, only meaningful if "set_mu_dft = true"	

mu_dc                                !Fixing the double-counting potential, only meaningful if "set_mu_dc = true"

n_ions                               !Number of ions in unit-cell of the material, has to be consistend with VASP precalculations

U_3d
J_3d

J

tol                                  !Convergence criterion for chemical potential and energy

Vectors:
 
M                                    !magnetic energy kick, must be of length n_ions
   
mu_int		                     !nested interval for mu-search, is alway of length 3

Logicals:	
	          
is_metallic			     !insulator, semiconductor or metal algorithm
   
use_para			     !use paramagnetic self-energy
   
use_t2g_eg_sym		             !symmetrize density matrix for each ion and spin sector

use_afm_sym			     !symmetrize density matrix antiferromagnetically wrt. ions

use_sym_ions		             !symmetrize density matrix wrt. ions
  
calc_G0_dos_only		     !flag to stop code after calculating G0 dos

use_sc_G			     !use self-consistent G

read_sigma			     !start with self-energy file in iteration 1
  
export_para			     !export paramagnetic self-energy

set_mu_dft                           !Set fixed value for the chemical potential manually

set_mu_dc                            !Set fixed value for the double-counting potential manually


Default values: The values have been arbitrarily chosen to be suited for calculations of NiO

beta = 100.0		
  
n_iw = 2100				
  
n_iter = 5                                    

mix_new_sigma = 0.5			          

idelta = 0.05                           

mu_dft =						

mu_dc = 

n_ions = 2

U_3d = 8.0

J_3d = 1.36
   
U = 8.00

J = 1.00

tol = 1.0D-8	


 
M 
   
mu_int = (/-30., 0., 30./)
	
is_metallic = .FALSE.		
   
use_para = .FALSE.			     
   
use_t2g_eg_sym = .TRUE.	
	   
use_afm_sym = .TRUE.
		
use_sym_ions = .FALSE.			  			
  
calc_G0_dos_only = .FALSE.	
	               
use_sc_G = .TRUE.	
		             
read_sigma = .TRUE.			      
  
export_para = .TRUE.			        

set_mu_dft = .FALSE.                            

set_mu_dc = .FALSE.                        
